<!DOCTYPE html>
<html lang="en">
<head>
	<title>SIGN IN</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?php echo site_url(); ?>assets/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>assets/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>assets/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>assets/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>assets/css/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>assets/css/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>assets/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>assets/css/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>assets/css/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>assets/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>assets/css/main.css">
<!--===============================================================================================-->
</head>
<body>
	<div class="limiter">
		<div class="container-login100" style="background-image: url('assets/images/bg-01.jpg');">
			<div class="wrap-login100">
				<form class="login100-form validate-form" action="<?php echo base_url();?>main/login" method="post">
					<span class="login100-form-logo">
						<i class="zmdi zmdi-landscape"></i>
					</span>
					<span class="login100-form-title p-b-34 p-t-27">
						SIGN IN
					</span>
					<?php if (validation_errors()): ?>
						<div class="alert alert-danger">
							<?php
							 echo validation_errors(); 
							 ?>
						</div>
					<?php endif; ?>
					
					<?php if ($this->session->flashdata('msg')): ?>
						<div class="alert alert-success" id="alert">
							<?php echo $this->session->flashdata('msg'); ?>
						</div>
					<?php endif; ?>
					
					<?php if ($this->session->flashdata('err')): ?>
						<div class="alert alert-danger" id="alert">
							<?php echo $this->session->flashdata('err'); ?>
						</div>
					<?php endif; ?>
					
					<div class="wrap-input100 validate-input" data-validate = "Enter username">
						<input class="input100" type="text" name="username" placeholder="username (NIP)">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<input class="input100" type="password" name="password" placeholder="Password">
						<span class="focus-input100" data-placeholder="&#xf191;"></span>
					</div>

					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							SIGN IN
						</button>
						<a href="<?php echo base_url();?>Main/signup" class="login100-form-btn">
							SIGN UP
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	

<!--===============================================================================================-->
	<script src="<?php echo site_url(); ?>assets/js/vendor/jquery-2.1.4.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo site_url(); ?>assets/jsvendor/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo site_url(); ?>assets/jsvendor/js/popper.js"></script>
	<script src="<?php echo site_url(); ?>assets/jsvendor/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo site_url(); ?>assets/jsvendor/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo site_url(); ?>assets/jsvendor/moment.min.js"></script>
	<script src="<?php echo site_url(); ?>assets/jsvendor/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo site_url(); ?>assets/jsvendor/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo site_url(); ?>assets/js/main.js"></script>

</body>
</html>
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Data Pegawai</h1>
            </div>
        </div>
    </div>
    <a href="<?php echo site_url('admin/create'); ?>" class="btn btn-primary pull-right" style="margin-right:25px; margin-top:5px; color:white; border-radius:5px;">Tambah Data Pegawai</a>
    <a href="<?php echo site_url('admin/print_data_pegawai'); ?>" target="_blank" class="btn btn-info pull-right" style="margin-right:25px; margin-top:5px; color:white; border-radius:5px;">Cetak Data Pegawai</a>
</div>
<?php if ($this->session->flashdata('msg')): ?>
  <div class="alert alert-success" id="alert" style="margin-top:7px;">
    <?php echo $this->session->flashdata('msg'); ?>
  </div>
<?php endif; ?>

<?php if ($this->session->flashdata('err')): ?>
  <div class="alert alert-danger" id="alert" style="margin-top:7px;">
    <?php echo $this->session->flashdata('err'); ?>
  </div>
<?php endif; ?>
<div class="content col-sm-12" style="background-color:white;">
<div class="table-responsive table-striped" style="margin-top:15px;">
  <table class="table" id="tblcuti">
    <thead class="text-center">
      <tr>
        <th>No</th>
        <th>NIP</th>
        <th>Nama</th>
        <th>Golongan</th>
        <th>Kelengkapan Data</th>
        <th>Aksi</th>
      </tr>
    </thead>
    <tbody>
      <?php $no = 1; foreach ($user as $value): ?>
        <tr>
          <td class="text-center text-nowrap"><?php echo $no++; ?></td>
          <td class="text-center text-nowrap"><?php echo $value['nip']; ?></td>
          <td class="text-nowrap"><?php echo $value['nama']; ?></td>
          <td class="text-center text-nowrap"><?php echo $value['golongan']; ?></td>
          <td class="text-center text-nowrap">
            <?php 
              if (($value['nip'] == NULL || $value['nip'] == "") ||
              ($value['nama'] == NULL || $value['nama'] == "") ||
              ($value['tempat_lahir'] == NULL || $value['tempat_lahir'] == "") ||
              ($value['tanggal_lahir'] == NULL || $value['tanggal_lahir'] == "") ||
              ($value['jk'] == NULL || $value['jk'] == "") ||
              ($value['status_keluarga'] == NULL || $value['status_keluarga'] == "") ||
              ($value['agama'] == NULL || $value['agama'] == "") ||
              ($value['golongan'] == NULL || $value['golongan'] == "") ||
              ($value['tmt_golongan'] == NULL || $value['tmt_golongan'] == "") ||
              ($value['jabatan_struktural'] == NULL || $value['jabatan_struktural'] == "") ||
              ($value['tmt_jabatan'] == NULL || $value['tmt_jabatan'] == "") ||
              ($value['tempat_tugas'] == NULL || $value['tempat_tugas'] == "") ||
              ($value['pendidikan_akhir'] == NULL || $value['pendidikan_akhir'] == "") ||
              ($value['jurusan'] == NULL || $value['jurusan'] == "") ||
              ($value['lth_penjenjangan'] == NULL || $value['lth_penjenjangan'] == "") ||
              ($value['tgl_capeg'] == NULL || $value['tgl_capeg'] == "") ||
              ($value['email'] == NULL || $value['email'] == "")) {
                
                $data = "Belum";
              }else {
                $data = "Lengkap";
              }
              
              echo $data;
              $id = $value['id'];
             ?>
          </td>
          <td class="text-center text-nowrap">
            <a href="<?php echo site_url("admin/profil/$id") ?>" class="btn btn-info btn-sm"> <i class="fa fa-search"></i> </a>
            <a href="<?php echo site_url("admin/edit/$id") ?>" class="btn btn-warning btn-sm"> <i class="fa fa-pencil"></i> </a>
            <a href="<?php echo site_url("admin/delete/$id") ?>" class="btn btn-danger btn-sm"> <i class="fa fa-trash"></i> </a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
</div>
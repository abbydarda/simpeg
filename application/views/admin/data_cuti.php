<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Data Cuti Pegawai</h1>
            </div>
        </div>
    </div>
    <a href="<?php echo site_url('admin/generate_cuti'); ?>" class="btn btn-primary pull-right" id="generate" style="margin-right:25px; margin-top:5px; color:white; border-radius:5px;">Generate Data Cuti</a>
</div>
<?php if ($this->session->flashdata('msg')): ?>
  <div class="alert alert-success" id="alert" style="margin-top:7px;">
    <?php echo $this->session->flashdata('msg'); ?>
  </div>
<?php endif; ?>

<?php if ($this->session->flashdata('err')): ?>
  <div class="alert alert-danger" id="alert" style="margin-top:7px;">
    <?php echo $this->session->flashdata('err'); ?>
  </div>
<?php endif; ?>
<div class="content col-sm-12" style="background-color:white;">
<div class="table-responsive table-striped" style="margin-top:15px;">
  <table class="table" id="tblcuti">
    <thead class="text-center">
      <tr>
        <th>No</th>
        <th>NIP</th>
        <th>Nama</th>
        <th>Sisa Cuti Tahunan</th>
        <th>Sisa Cuti Bersalin</th>
        <th>Jumlah Cuti Tahunan</th>
        <th>Jumlah Cuti Bersalin</th>
      </tr>
    </thead>
    <tbody>
      <?php $no=1; foreach ($cuti as $val): ?>
        <tr>
          <td class="text-center text-nowrap"><?php echo $no++; ?></td>
          <td class="text-center text-nowrap"><?php echo $val['nip']; ?></td>
          <td class="text-nowrap"><?php echo $val['nama']; ?></td>
          <td class="text-center text-nowrap"><?php echo (isset($val['sisa_cuti_tahunan'])) ? $val['sisa_cuti_tahunan'] : 0 ; ?></td>
          <td class="text-center text-nowrap"><?php echo (isset($val['sisa_cuti_bersalin'])) ? $val['sisa_cuti_bersalin'] : 0 ; ?></td>
          <td class="text-center text-nowrap"><?php echo $val['jumlah_cuti_tahunan']; ?></td>
          <td class="text-center text-nowrap"><?php echo $val['jumlah_cuti_bersalin']; ?></td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
</div>
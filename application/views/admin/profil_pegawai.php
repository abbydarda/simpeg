<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Profil Pegawai</h1>
            </div>
        </div>
    </div>
    <?php $id = $this->uri->segment(3); ?>
    <?php if ($this->session->userdata('hak') == 1): ?>
      <a href="<?php echo site_url("admin/delete/$id"); ?>" class="btn btn-danger pull-right" style="margin-right:5px; margin-top:5px; color:white; border-radius:5px;"> <i class="fa fa-trash"></i> </a>
    <?php else: ?>
      <a style="display:none;" href="<?php echo site_url("admin/delete/$id"); ?>" class="btn btn-danger pull-right" style="margin-right:5px; margin-top:5px; color:white; border-radius:5px;"> <i class="fa fa-trash"></i> </a>
    <?php endif; ?>
    <a href="<?php echo site_url("admin/edit/$id"); ?>" class="btn btn-warning pull-right" style="margin-right:5px; margin-top:5px; color:white; border-radius:5px;"> <i class="fa fa-pencil"></i> </a>
    <?php if ($this->session->userdata('hak') == 1): ?>
      <a href="<?php echo site_url("admin/user"); ?>" class="btn btn-info pull-right" style="margin-right:5px; margin-top:5px; color:white; border-radius:5px;"> <i class="fa fa-arrow-left"></i> </a>
    <?php else: ?>
      <a href="<?php echo site_url("admin/index"); ?>" class="btn btn-info pull-right" style="margin-right:5px; margin-top:5px; color:white; border-radius:5px;"> <i class="fa fa-arrow-left"></i> </a>
    <?php endif; ?>
</div>
<?php if ($this->session->flashdata('msg')): ?>
  <div class="alert alert-success" id="alert" style="margin-top:7px;">
    <?php echo $this->session->flashdata('msg'); ?>
  </div>
<?php endif; ?>

<?php if ($this->session->flashdata('err')): ?>
  <div class="alert alert-danger" id="alert" style="margin-top:7px;">
    <?php echo $this->session->flashdata('err'); ?>
  </div>
<?php endif; ?>
<div class="content col-sm-12" style="background-color:white;">
    <?php foreach ($user as $value): 
        $tahun_lahir = substr($value['tanggal_lahir'], 0,4);
        $bulan_lahir = substr($value['tanggal_lahir'], 5,2);
        $tanggal_lahir = substr($value['tanggal_lahir'], 8,2);
        
        switch ($bulan_lahir) {
          case '01':
            $bulan = "Januari";
            break;
          case '02':
            $bulan = "Februari";
            break;
          case '03':
            $bulan = "Maret";
            break;
          case '04':
            $bulan = "April";
            break;
          case '05':
            $bulan = "Mei";
            break;
          case '06':
            $bulan = "Juni";
            break;
          case '07':
            $bulan = "Juli";
            break;
          case '08':
            $bulan = "Agustus";
            break;
          case '09':
            $bulan = "September";
            break;  
          case '10':
            $bulan = "Oktober";
            break;
          case '11':
            $bulan = "November";
            break;
          case '12':
            $bulan = "Desember";
            break;
        }
    ?>
      
      <div class="col-sm-2">
        <?php if ($value['foto'] != NULL || $value['foto'] != ""): ?>
          <img src="<?php echo $value['foto']; ?>" alt="">
        <?php else: ?>
          <img style="margin-top:15px;" src="../../../assets/images/pegawai/default.png" class="rounded mx-auto d-block" alt="...">
        <?php endif; ?>
      </div>
      <div class="table-responsive col-sm-8">
        <table class="table">
          <tr>
            <td class="text-nowrap" style="width:5%;">NIP</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap"><?php echo $value['nip']; ?></td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">No Karpeg</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap"><?php echo ($value['no_karpeg'] != null || $value['no_karpeg'] != "") ? $value['no_karpeg'] : '-'; ?></td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">Nama</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap"><?php echo $value['nama']; ?></td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">Tempat,Tanggal Lahir</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap"><?php echo $value['tempat_lahir'].', '.$tanggal_lahir.' '.$bulan.' '.$tahun_lahir; ?></td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">Jenis Kelamin</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap"><?php echo ($value['jk'] == 0) ? 'Perempuan' : 'Laki-Laki' ;?></td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">Status Keluarga</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap"><?php echo ($value['status_keluarga'] == 1) ? 'Belum Nikah' : 'Nikah' ;?></td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">Agama</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap">
              <?php 
               switch ($value['agama']) {
                 case '1':
                   echo "Islam";
                   break;
                 case '2':
                   echo "Kristen Protestan";
                   break;
                 case '3':
                   echo "Katolik";
                   break;
                 case '4':
                   echo "Hindu";
                   break;
                 case '5':
                   echo "Buddha";
                   break;
                 case '6':
                   echo "Kong Hu Cu*";
                   break;
               }
               ?>
            </td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">Golongan</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap"><?php echo $value['golongan'];?></td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">TMT Golongan</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap"><?php echo ($value['tmt_golongan'] == null || $value['tmt_golongan'] == "") ? '-' : date('d-m-Y',strtotime($value['tmt_golongan']));?></td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">MK Golongan (thn bln)</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap">
              <?php 
                $mk_gol_thn = ($value['mk_golongan_thn'] != NULL || $value['mk_golongan_thn'] != "NULL") ? $value['mk_golongan_thn'] : '-' ;
                $mk_gol_bulan = ($value['mk_golongan_bulan'] != NULL || $value['mk_golongan_bulan'] != "NULL") ? $value['mk_golongan_bulan'] : '-' ;
                 echo $mk_gol_thn.' Tahun '.$mk_gol_bulan.' Bulan';
               ?>
            </td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">Jabatan Struktural</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap"><?php echo ($value['jabatan_struktural'] == null || $value['jabatan_struktural'] == "") ? '-' : $value['jabatan_struktural'] ;?></td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">Jabatan Fungsional</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap"><?php echo ($value['jabatan_fungsional'] == null || $value['jabatan_fungsional'] == "") ? '-' : $value['jabatan_fungsional'] ;?></td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">TMT Struktural / TMT Fngsional</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap"><?php echo ($value['tmt_jabatan'] == null || $value['tmt_jabatan'] == "") ? '- -' : $value['tmt_jabatan'] ;?></td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">Tempat Dinas</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap"><?php echo ($value['tempat_tugas'] == null || $value['tempat_tugas'] == "") ? '-' : $value['tempat_tugas'] ;?></td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">Pendidikan Akhir</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap"><?php echo ($value['pendidikan_akhir'] == null || $value['pendidikan_akhir'] == "") ? '-' : $value['pendidikan_akhir'] ;?></td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">Jurusan</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap"><?php echo ($value['jurusan'] == null || $value['jurusan'] == "") ? '-' : $value['jurusan'] ;?></td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">LTH Penjenjangan</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap"><?php echo ($value['lth_penjenjangan'] == null || $value['lth_penjenjangan'] == "") ? '-' : $value['lth_penjenjangan'] ;?></td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">Tanggal Capeg</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap"><?php echo ($value['tgl_capeg'] == null || $value['tgl_capeg'] == "") ? '-' : date('d-m-Y',strtotime($value['tgl_capeg'])) ;?></td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">MK Keseluruhan (thn bln)</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap">
              <?php 
                $mk_kesel_thn = ($value['mk_kesel_thn'] != NULL || $value['mk_kesel_thn'] != "NULL") ? $value['mk_kesel_thn'] : '-' ;
                $mk_kesel_bln = ($value['mk_kesel_bln'] != NULL || $value['mk_kesel_bln'] != "NULL") ? $value['mk_kesel_bln'] : '-' ;
                 echo $mk_kesel_thn.' Tahun '.$mk_kesel_bln.' Bulan';
               ?>
            </td>
          </tr>
        </table>  
      </div>
    <?php endforeach; ?>
</div>
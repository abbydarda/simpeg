<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Form Tambah Pegawai</h1>
            </div>
        </div>
    </div>
</div>
<?php if (validation_errors()): ?>
  <div class="alert alert-danger">
    <?php
     echo validation_errors(); 
     ?>
  </div>
<?php endif; ?>

<?php if ($this->session->flashdata('msg')): ?>
  <div class="alert alert-success" id="alert" style="margin-top:7px;">
    <?php echo $this->session->flashdata('msg'); ?>
  </div>
<?php endif; ?>

<?php if ($this->session->flashdata('err')): ?>
  <div class="alert alert-danger" id="alert" style="margin-top:7px;">
    <?php echo $this->session->flashdata('err'); ?>
  </div>
<?php endif; $id = $this->session->userdata('id');?>
<div class="content col-sm-12" style="background-color:white;">
  <form class="form-group" action="<?php echo site_url("admin/create");?>" method="post" enctype="multipart/form-data">
    
    <div class="col-sm-2">
        <img style="margin-top:15px;" src="<?php echo site_url();?>assets/images/pegawai/default.png" class="rounded mx-auto d-block" alt="...">
        <input type="file" name="userfile" class="form-control">
    </div>
    <div class="table-responsive col-sm-8">
      <table class="table">
        <tr>
          <td class="text-nowrap" style="width:10px;">NIP</td>
          <td class="text-nowrap" style="width:20px;">:</td>
          <td class="text-nowrap"> <input class="form-control" type="text" name="nip" value="<?php echo set_value('nip'); ?>"> </td>
        </tr>
        <tr>
          <td class="text-nowrap" style="width:5%;">No Karpeg</td>
          <td class="text-nowrap" style="width:1%;">:</td>
          <td class="text-nowrap"> <input class="form-control" type="text" name="no_karpeg" value="<?php echo set_value('no_karpeg');?>"> </td>
        </tr>
        <tr>
          <td class="text-nowrap" style="width:5%;">Nama</td>
          <td class="text-nowrap" style="width:1%;">:</td>
          <td class="text-nowrap"> <input type="text" name="nama" value="<?php echo set_value('nama'); ?>" class="form-control"> </td>
        </tr>
        <tr>
          <td class="text-nowrap" style="width:5%;">Tempat Lahir</td>
          <td class="text-nowrap" style="width:1%;">:</td>
          <td class="text-nowrap"> <input type="text" name="tempat_lahir" value="<?php echo set_value('tempat_lahir'); ?>" class="form-control"></td>
        </tr>
        <tr>
          <td class="text-nowrap" style="width:5%;">Tanggal Lahir</td>
          <td class="text-nowrap" style="width:1%;">:</td>
          <td class="text-nowrap"><input type="date" name="tanggal_lahir" value="<?php echo set_value('tanggal_lahir'); ?>" class="form-control"> </td>
        </tr>
        <tr>
          <td class="text-nowrap" style="width:5%;">Jenis Kelamin</td>
          <td class="text-nowrap" style="width:1%;">:</td>
          <td class="text-nowrap">
            <select class="form-control" name="jk">
              <option value="">Pilih Jenis Kelamin</option>
              <option value="1" <?php echo set_select('jk', 1, False); ?>>Laki-Laki</option>
              <option value="0" <?php echo set_select('jk', 0, False); ?>>Perempuan</option>
            </select>
          </td>
        </tr>
        <tr>
          <td class="text-nowrap" style="width:5%;">Status Keluarga</td>
          <td class="text-nowrap" style="width:1%;">:</td>
          <td class="text-nowrap">
            <select class="form-control" name="status">
              <option value="">Pilih Status Keluarga</option>
              <option value="1" <?php echo set_select('status', 0, False); ?>>Belum Nikah</option>
              <option value="2" <?php echo set_select('status', 1, False); ?>>Nikah</option>
            </select>
          </td>
        </tr>
        <tr>
          <td class="text-nowrap" style="width:5%;">Agama</td>
          <td class="text-nowrap" style="width:1%;">:</td>
          <td class="text-nowrap">
            <select class="form-control" name="agama">
              <option value="">Pilih Agama</option>
              <option value="1" <?php echo set_select('agama', 1, False); ?>>Islam</option>
              <option value="2" <?php echo set_select('agama', 2, False); ?>>Kristen Protestan</option>
              <option value="3" <?php echo set_select('agama', 3, False); ?>>Katolik</option>
              <option value="4" <?php echo set_select('agama', 4, False); ?>>Hindu</option>
              <option value="5" <?php echo set_select('agama', 5, False); ?>>Buddha</option>
              <option value="6" <?php echo set_select('agama', 6, False); ?>>Kong Hu Cu</option>
            </select>
          </td>
        </tr>
        <tr>
          <td class="text-nowrap" style="width:5%;">Golongan</td>
          <td class="text-nowrap" style="width:1%;">:</td>
          <td class="text-nowrap">
            <input type="text" name="golongan" value="<?php echo set_value('golongan');?>" class="form-control">
          </td>
        </tr>
        <tr>
          <td class="text-nowrap" style="width:5%;">TMT Golongan</td>
          <td class="text-nowrap" style="width:1%;">:</td>
          <td class="text-nowrap">
            <input type="date" name="tmt_golongan" value="<?php echo set_value('tmt_golongan');?>" class="form-control">
          </td>
        </tr>
        <tr>
          <td class="text-nowrap" style="width:5%;">MK Golongan Tahun</td>
          <td class="text-nowrap" style="width:1%;">:</td>
          <td class="text-nowrap">
            <input type="text" name="mk_golongan_thn" value="<?php echo set_value('mk_golongan_thn');?>" class="form-control">
          </td>
        </tr>
        <tr>
          <td class="text-nowrap" style="width:5%;">MK Golongan Bulan</td>
          <td class="text-nowrap" style="width:1%;">:</td>
          <td class="text-nowrap">
            <input type="text" name="mk_golongan_bulan" value="<?php echo set_value('mk_golongan_bulan');?>" class="form-control">
          </td>
        </tr>
        <tr>
          <td class="text-nowrap" style="width:5%;">Jabatan Struktural</td>
          <td class="text-nowrap" style="width:1%;">:</td>
          <td class="text-nowrap">
            <input class="form-control" type="text" name="jabatan_struktural" value="<?php echo set_value('jabatan_struktural'); ?>">
          </td>
        </tr>
        <tr>
          <td class="text-nowrap" style="width:5%;">Jabatan Fungsional</td>
          <td class="text-nowrap" style="width:1%;">:</td>
          <td class="text-nowrap">
            <input class="form-control" type="text" name="jabatan_fungsional" value="<?php echo set_value('jabatan_fungsional'); ?>">
          </td>
        </tr>
        <tr>
          <td class="text-nowrap" style="width:5%;">TMT Struktural / TMT Fngsional</td>
          <td class="text-nowrap" style="width:1%;">:</td>
          <td class="text-nowrap">
            <input class="form-control" type="date" name="tmt_jabatan" value="<?php echo set_value('tmt_jabatan'); ?>">
          </td>
        </tr>
        <tr>
          <td class="text-nowrap" style="width:5%;">Tempat Dinas</td>
          <td class="text-nowrap" style="width:1%;">:</td>
          <td class="text-nowrap">
            <input class="form-control" type="text" name="tempat_tugas" value="<?php echo set_value('tempat_tugas'); ?>">
          </td>
        </tr>
        <tr>
          <td class="text-nowrap" style="width:5%;">Pendidikan Akhir</td>
          <td class="text-nowrap" style="width:1%;">:</td>
          <td class="text-nowrap">
            <input class="form-control" type="text" name="pendidikan_akhir" value="<?php echo set_value('pendidikan_akhir'); ?>">
          </td>
        </tr>
        <tr>
          <td class="text-nowrap" style="width:5%;">Jurusan</td>
          <td class="text-nowrap" style="width:1%;">:</td>
          <td class="text-nowrap">
            <input class="form-control" type="text" name="jurusan" value="<?php echo set_value('jurusan'); ?>">
          </td>
        </tr>
        <tr>
          <td class="text-nowrap" style="width:5%;">LTH Penjenjangan</td>
          <td class="text-nowrap" style="width:1%;">:</td>
          <td class="text-nowrap">
            <input class="form-control" type="text" name="lth_penjenjangan" value="<?php echo set_value('lth_penjenjangan'); ?>">
          </td>
        </tr>
        <tr>
          <td class="text-nowrap" style="width:5%;">Tanggal Capeg</td>
          <td class="text-nowrap" style="width:1%;">:</td>
          <td class="text-nowrap">
            <input class="form-control" type="date" name="tgl_capeg" value="<?php echo set_value('tgl_capeg'); ?>">
          </td>
        </tr>
        <tr>
          <td class="text-nowrap" style="width:5%;">MK Keseluruhan Tahun</td>
          <td class="text-nowrap" style="width:1%;">:</td>
          <td class="text-nowrap">
            <input type="text" name="mk_kesel_thn" value="<?php echo set_value('mk_kesel_thn');?>" class="form-control">
          </td>
        </tr>
        <tr>
          <td class="text-nowrap" style="width:5%;">MK Keseluruhan Bulan</td>
          <td class="text-nowrap" style="width:1%;">:</td>
          <td class="text-nowrap">
            <input type="text" name="mk_kesel_bln" value="<?php echo set_value('mk_kesel_bln');?>" class="form-control">
          </td>
        </tr>
        <tr>
          <td class="text-nowrap" style="width:5%;">Hak Akses</td>
          <td class="text-nowrap" style="width:1%;">:</td>
          <td class="text-nowrap">
            <select class="form-control" name="hak_akses">
              <option value="">Pilih Hak Akses</option>
              <option value="1" <?php echo set_select('hak_akses', 1, False); ?>>Admin</option>
              <option value="2" <?php echo set_select('hak_akses', 2, False); ?>>Pegawai</option>
              <option value="3" <?php echo set_select('hak_akses', 3, False); ?>>Pemimpin</option>
            </select>
          </td>
        </tr>
        <tr>
          <td class="text-nowrap" style="width:5%;">Email</td>
          <td class="text-nowrap" style="width:1%;">:</td>
          <td class="text-nowrap">
            <input class="form-control" type="email" name="email" value="<?php echo set_value('email'); ?>">
          </td>
        </tr>
        <tr>
          <td class="text-nowrap" style="width:5%;">Password</td>
          <td class="text-nowrap" style="width:1%;">:</td>
          <td class="text-nowrap">
            <input class="form-control" type="password" name="password" value="">
          </td>
        </tr>
        <tr>
          <td> </td>
          <td> </td>
          <td>
            <input type="submit" name="update" value="Tambah" class="btn btn-primary pull-right" style="margin-left:10px;">
            <a class="btn btn-warning pull-right" href="<?php echo site_url('admin/user');?>">Kembali</a>
          </td>
        </tr>
      </table>  
    </div>
  </form>
</div>
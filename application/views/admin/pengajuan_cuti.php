<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Pengajuan Cuti</h1>
            </div>
        </div>
    </div>
    <a class="btn btn-primary pull-right" id="ajukan_cuti" style="margin-right:25px; margin-top:5px; color:white; border-radius:5px;">Ajukan Cuti</a>
</div>
<?php if (validation_errors()): ?>
  <div class="alert alert-danger">
    <?php
     echo validation_errors(); 
     ?>
  </div>
<?php endif; ?>

<?php if ($this->session->flashdata('msg')): ?>
  <div class="alert alert-success" id="alert" style="margin-top:7px;">
    <?php echo $this->session->flashdata('msg'); ?>
  </div>
<?php endif; ?>

<?php if ($this->session->flashdata('err')): ?>
  <div class="alert alert-danger" id="alert" style="margin-top:7px;">
    <?php echo $this->session->flashdata('err'); ?>
  </div>
<?php endif; ?>

<div class="card" id="form_cuti" style="display:none;">
  <div class="card-header" style="background-color:rgb(10,0,130);">
    <span style="color:white;">Formulir Pengajuan Cuti</span>
    <a class="pull-right" id="close"> <i class="fa fa-times-circle" style="color:white;"></i> </a>
  </div>
  <div class="card-body">
    <?php $id = $this->session->userdata('id'); ?>
    <?php if ($sisa_cuti['jumlah_cuti_tahunan']  != 0): ?>
      <form class="form-group" action="<?php echo site_url("admin/create_pengajuan_cuti/$id") ?>" method="post">
        <table class="table">
          <tr>
            <td>Tanggal Pengajuan</td>
            <td>:</td>
            <td> <input class="form-control" type="text" name="tanggal_pengajuan" value="<?php echo date("d-m-Y"); ?>" readonly> </td>
          </tr>
          <tr>
            <td>Jenis Cuti</td>
            <td>:</td>
            <td>
              <?php if ($this->session->userdata('jk') == 1): ?>
                <select class="form-control" name="jenis_cuti">
                  <option value="">Pilih Jenis Cuti</option>
                  <option value="tahunan" <?php echo set_select('jenis_cuti', 'tahunan', False); ?>>Cuti Tahunan</option>
                  <option value="besar" <?php echo set_select('jenis_cuti', 'a', False); ?>>Cuti Besar</option>
                  <option value="sakit" <?php echo set_select('jenis_cuti', 'b', False); ?>>Cuti Sakit</option>
                  <option value="alasan" <?php echo set_select('jenis_cuti', 'c', False); ?>>Cuti Karena Alasan Penting</option>
                  <option value="lain" <?php echo set_select('jenis_cuti', 'c', False); ?>>Keterang Lain-Lain</option>
                </select>
              <?php else: ?>
                <select class="form-control" name="jenis_cuti">
                  <option value="">Pilih Jenis Cuti</option>
                  <option value="tahunan" <?php echo set_select('jenis_cuti', 'tahunan', False); ?>>Cuti Tahunan</option>
                  <option value="besar" <?php echo set_select('jenis_cuti', 'a', False); ?>>Cuti Besar</option>
                  <option value="sakit" <?php echo set_select('jenis_cuti', 'b', False); ?>>Cuti Sakit</option>
                  <option value="bersalin" <?php echo set_select('jenis_cuti', 'bersalin', False); ?>>Cuti Bersalin</option>
                  <option value="alasan" <?php echo set_select('jenis_cuti', 'c', False); ?>>Cuti Karena Alasan Penting</option>
                  <option value="lain" <?php echo set_select('jenis_cuti', 'c', False); ?>>Keterang Lain-Lain</option>
                </select>
              <?php endif; ?>
            </td>
          </tr>
          <tr>
            <td>Tangga Awal Cuti</td>
            <td>:</td>
            <td> <input id="tanggal_awal_cuti" type="date" name="tanggal_awal" value="<?php echo set_value('tanggal_awal'); ?>" class="form-control"></td>
          </tr>
          <tr>
            <td>Tangga Akhir Cuti</td>
            <td>:</td>
            <td> <input id="tanggal_akhir_cuti" type="date" name="tanggal_akhir" value="<?php echo set_value('tanggal_akhir'); ?>" class="form-control"></td>
          </tr>
          <tr>
            <td>Jumlah Hari</td>
            <td>:</td>
            <td> <input id="jumlah_hari" type="text" name="jumlah_hari" value="<?php echo set_value('jumlah_hari'); ?>" class="form-control" readonly> </td>
          </tr>
          <tr>
            <td>Pimpinan</td>
            <td>:</td>
            <td> 
              <select class="form-control" name="id_pimpinan">
                <option value="">Pilih Pimpinan</option>
                <?php foreach ($pemimpin as $value): ?>
                  <option value="<?php echo $value['id']; ?>"><?php echo $value['nip'].' - '.$value['nama']; ?></option>
                <?php endforeach; ?>
              </select>
            </td>
          </tr>
          <tr>
            <td>Pejabat</td>
            <td>:</td>
            <td> 
              <select class="form-control" name="id_pejabat">
                <option value="">Pilih Pejabat</option>
                <?php foreach ($pejabat as $value): ?>
                  <option value="<?php echo $value['id']; ?>"><?php echo $value['nip'].' - '.$value['nama']; ?></option>
                <?php endforeach; ?>
              </select>
            </td>
          </tr>
          <tr>
            <td>Dinas</td>
            <td>:</td>
            <td> 
              <select class="form-control" name="dinas">
                <option value="">Pilih Dinas</option>
                <option value="1">Tanaman Pangan</option>
                <option value="2">Tanaman Hortikultura</option>
                <option value="3">Prasarana dan Sarana</option>
                <option value="4">Penyuluhan</option>
              </select>
            </td>
          </tr>

          <tr>
            <td>Sisa Cuti</td>
            <td>:</td>
            <td> <input id="sisa_cuti" type="text" name="sisa_cuti" value="<?php echo $sisa_cuti['jumlah_cuti_tahunan']; ?>" class="form-control" readonly> </td>
          </tr>
          <tr>
            <td>
            <input class="btn btn-primary" type="submit" name="submit" value="Kirim">
            </td>
          </tr>
        </table>
      </form>
      <?php else: ?>
        <div class="alert alert-danger text-center">
          Maaf Sisa Cuti Anda Sudah Habis / Belum Dimasukan Oleh Admin
        </div>
    <?php endif; ?>
  </div> 
</div>

<div class="content col-sm-12" style="background-color:white;">
  <div class="table-responsive table-striped" style="margin-top:15px;">
    <table class="table" id="tblpengajuan">
      <thead class="text-center">
        <th>NO</th>
        <th>NIP</th>
        <th>Nama</th>
        <th>Tanggal Pengajuan</th>
        <th>Tanggal Awal Cuti</th>
        <th>Tanggal Akhir Cuti</th>
        <th>Jumlah Hari</th>
        <th>Status Pengajuan</th>
        <?php if ($this->session->userdata('hak') == 1 || $this->session->userdata('hak') == 3): ?>
        <th>Aksi</th>
        <?php endif; ?>
      </thead>
      <tbody>
        <?php $no = 1; foreach ($pengajuan as $value): ?>
          <tr>
            <td class="text-center text-nowrap"><?php echo $no++; ?></td>
            <td class="text-center text-nowrap"><?php echo $value['nip']; ?></td>
            <td><?php echo $value['nama']; ?></td>
            <td class="text-center text-nowrap"><?php echo date('d-m-Y',strtotime($value['tanggal_pengajuan'])); ?></td>
            <td class="text-center text-nowrap"><?php echo date('d-m-Y',strtotime($value['tanggal_awal'])); ?></td>
            <td class="text-center text-nowrap"><?php echo date('d-m-Y',strtotime($value['tanggal_akhir'])); ?></td>
            <td class="text-center text-nowrap"><?php echo $value['jumlah_hari']; ?></td>
            <td class="text-center text-nowrap"><?php echo ($value['status_pengajuan'] == 0) ? "Menunggu" : (($value['status_pengajuan'] == 1) ? "Disetujui" : "Ditolak"); ?></td>
            <?php if ($this->session->userdata('hak') == 1 || $this->session->userdata('hak') == 3): ?>
            <td class="text-center text-nowrap">
              <?php if ($this->session->userdata('hak') == 1): ?>
                <?php if ($value['status_pengajuan'] == 1): ?>
                  <a href="<?php echo site_url();?>admin/print_surat_pengajuan/<?php echo $value['id']; ?>" target ="_blank" class="btn btn-info btn-sm"> <i class="fa fa-print"></i> </a>
                <?php else: ?>
                  <a class="btn btn-info btn-sm" disabled> <i class="fa fa-print"></i> </a>
                <?php endif; ?>
              <?php elseif ($this->session->userdata('hak') == 3): ?>
                <?php if ($value['status_pengajuan'] == 1): ?>
                  <a class="btn btn-success btn-sm"> <i class="fa fa-check"></i> </a>
                <?php else: ?>
                  <a href="<?php echo site_url();?>admin/update_pengajuan/1/<?php echo $value['id'];?>" class="btn btn-success btn-sm"> <i class="fa fa-check"></i> </a>
                <?php endif; ?>
                <!-- <a href="<?php echo site_url();?>admin/update_pengajuan/2/<?php echo $value['id'];?>" class="btn btn-danger btn-sm"> <i class="fa fa-times"></i> </a> -->
              <?php endif; ?>
            </td>
            <?php endif; ?>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
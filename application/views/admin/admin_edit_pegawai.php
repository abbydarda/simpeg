<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Form Edit Pegawai</h1>
            </div>
        </div>
    </div>
</div>
<?php if (validation_errors()): ?>
  <div class="alert alert-danger">
    <?php
     echo validation_errors(); 
     ?>
  </div>
<?php endif; ?>

<?php if ($this->session->flashdata('msg')): ?>
  <div class="alert alert-success" id="alert" style="margin-top:7px;">
    <?php echo $this->session->flashdata('msg'); ?>
  </div>
<?php endif; ?>

<?php if ($this->session->flashdata('err')): ?>
  <div class="alert alert-danger" id="alert" style="margin-top:7px;">
    <?php echo $this->session->flashdata('err'); ?>
  </div>
<?php endif; $id = $this->uri->segment(3);?>
<div class="content col-sm-12" style="background-color:white;">
  <form class="form-group" action="<?php echo site_url("admin/update_pegawai/$id");?>" method="post" enctype="multipart/form-data">
    <?php foreach ($user as $value): 
        $tahun_lahir = substr($value['tanggal_lahir'], 0,4);
        $bulan_lahir = substr($value['tanggal_lahir'], 5,2);
        $tanggal_lahir = substr($value['tanggal_lahir'], 8,2);
        
        switch ($bulan_lahir) {
          case '01':
            $bulan = "Januari";
            break;
          case '02':
            $bulan = "Februari";
            break;
          case '03':
            $bulan = "Maret";
            break;
          case '04':
            $bulan = "April";
            break;
          case '05':
            $bulan = "Mei";
            break;
          case '06':
            $bulan = "Juni";
            break;
          case '07':
            $bulan = "Juli";
            break;
          case '08':
            $bulan = "Agustus";
            break;
          case '09':
            $bulan = "September";
            break;  
          case '10':
            $bulan = "Oktober";
            break;
          case '11':
            $bulan = "November";
            break;
          case '12':
            $bulan = "Desember";
            break;
        }
    ?>
      
      <div class="col-sm-2">
        <?php if ($value['foto'] != NULL || $value['foto'] != ""): ?>
          <img src="<?php echo site_url().$value['foto']; ?>" alt="">
        <?php else: ?>
          <img style="margin-top:15px;" src="<?php echo site_url(); ?>assets/images/pegawai/default.png" class="rounded mx-auto d-block" alt="...">
        <?php endif; ?>
        <input type="file" name="userfile" class="form-control" value="<?php echo site_url().$value['foto']; ?>">
        <input type="hidden" name="foto" class="form-control" value="<?php echo $value['foto']; ?>">
      </div>
      <div class="table-responsive col-sm-8">
        <table class="table">
          <tr>
            <td class="text-nowrap" style="width:10px;">NIP</td>
            <td class="text-nowrap" style="width:20px;">:</td>
            <td class="text-nowrap"> <input class="form-control" type="text" name="nip" value="<?php echo $value['nip']; ?>" readonly> </td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">No Karpeg</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap"> <input class="form-control" type="text" name="no_karpeg" value="<?php echo ($value['no_karpeg'] != null || $value['no_karpeg'] != "") ? $value['no_karpeg'] : '-'; ?>"> </td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">Nama</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap"> <input type="text" name="nama" value="<?php echo $value['nama']; ?>" class="form-control"> </td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">Tempat Lahir</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap"> <input type="text" name="tempat_lahir" value="<?php echo $value['tempat_lahir']; ?>" class="form-control"></td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">Tanggal Lahir</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap"><input type="date" name="tanggal_lahir" value="<?php echo $value['tanggal_lahir']; ?>" class="form-control"> </td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">Jenis Kelamin</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap">
              <select class="form-control" name="jk">
                <?php if ($value['jk'] == 1): ?>
                  <option value="<?php echo $value['jk'];?>" selected> <?php echo ($value['jk'] == 0) ? 'Perempuan' : 'Laki-Laki' ; ?> </option>
                  <option value="0">Perempuan</option>
                <?php else: ?>
                  <option value="<?php echo $value['jk'];?>" selected> <?php echo ($value['jk'] == 0) ? 'Perempuan' : 'Laki-Laki' ; ?> </option>
                  <option value="1">Laki-Laki</option>
                <?php endif; ?>
              </select>
            </td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">Status Keluarga</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap">
              <select class="form-control" name="status">
                <?php if ($value['status_keluarga'] == 1): ?>
                  <option value="<?php echo $value['status_keluarga'];?>" selected> <?php echo ($value['status_keluarga'] == 1) ? 'Belum Nikah' : 'Nikah' ; ?> </option>
                  <option value="2">Nikah</option>
                <?php else: ?>
                  <option value="<?php echo $value['status_keluarga'];?>" selected> <?php echo ($value['status_keluarga'] == 2) ? 'Nikah' : 'Belum Nikah' ; ?> </option>
                  <option value="1">Belum Nikah</option>
                <?php endif; ?>
              </select>
            </td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">Agama</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap">
              <select class="form-control" name="agama">
                <option value="<?php echo $value['agama']?>">
                  <?php 
                   switch ($value['agama']) {
                     case '1':
                       echo "Islam";
                       break;
                     case '2':
                       echo "Kristen Protestan";
                       break;
                     case '3':
                       echo "Katolik";
                       break;
                     case '4':
                       echo "Hindu";
                       break;
                     case '5':
                       echo "Buddha";
                       break;
                     case '6':
                       echo "Kong Hu Cu*";
                       break;
                   }
                   ?>
                </option>
                <option value="1" <?php echo set_select('agama', 1, False); ?> style="color:black;">Islam</option>
                <option value="2" <?php echo set_select('agama', 2, False); ?> style="color:black;">Kristen Protestan</option>
                <option value="3" <?php echo set_select('agama', 3, False); ?> style="color:black;">Katolik</option>
                <option value="4" <?php echo set_select('agama', 4, False); ?> style="color:black;">Hindu</option>
                <option value="5" <?php echo set_select('agama', 5, False); ?> style="color:black;">Buddha</option>
                <option value="6" <?php echo set_select('agama', 6, False); ?> style="color:black;">Kong Hu Cu</option>
              </select>
            </td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">Golongan</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap">
              <?php if ($this->session->userdata('hak') == 1): ?>
                <input class="form-control" type="text" name="golongan" value="<?php echo $value['golongan'];?>">
              <?php else: ?>
                <input class="form-control" type="text" name="golongan" value="<?php echo $value['golongan'];?>" readonly>
              <?php endif; ?>
            </td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">TMT Golongan</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap">
              <?php if ($this->session->userdata('hak') == 1): ?>
                <input class="form-control" type="date" name="tmt_golongan" value="<?php echo $value['tmt_golongan'];?>">
              <?php else: ?>
                <input class="form-control" type="date" name="tmt_golongan" value="<?php echo $value['tmt_golongan'];?>" readonly>
              <?php endif; ?>
            </td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">MK Golongan Tahun</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap">
              <?php if ($this->session->userdata('hak') == 1): ?>
                <input class="form-control" type="text" name="mk_golongan_thn" value="<?php echo $value['mk_golongan_thn'];?>">
              <?php else: ?>
                <input class="form-control" type="text" name="mk_golongan_thn" value="<?php echo $value['mk_golongan_thn'];?>" readonly>
              <?php endif; ?>
            </td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">MK Golongan Bulan</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap">
              <?php if ($this->session->userdata('hak') == 1): ?>
                <input class="form-control" type="text" name="mk_golongan_bulan" value="<?php echo $value['mk_golongan_bulan'];?>">
              <?php else: ?>
                <input class="form-control" type="text" name="mk_golongan_bulan" value="<?php echo $value['mk_golongan_bulan'];?>" readonly>
              <?php endif; ?>
            </td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">Jabatan Struktural</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap">
              <input class="form-control" type="text" name="jabatan_struktural" value="<?php echo $value['jabatan_struktural']; ?>">
            </td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">Jabatan Fungsional</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap">
              <input class="form-control" type="text" name="jabatan_fungsional" value="<?php echo $value['jabatan_fungsional']; ?>">
            </td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">TMT Struktural / TMT Fngsional</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap">
              <input class="form-control" type="date" name="tmt_jabatan" value="<?php echo $value['tmt_jabatan']; ?>">
            </td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">Tempat Dinas</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap">
              <input class="form-control" type="text" name="tempat_tugas" value="<?php echo $value['tempat_tugas']; ?>">
            </td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">Pendidikan Akhir</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap">
              <input class="form-control" type="text" name="pendidikan_akhir" value="<?php echo $value['pendidikan_akhir']; ?>">
            </td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">Jurusan</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap">
              <input class="form-control" type="text" name="jurusan" value="<?php echo $value['jurusan']; ?>">
            </td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">LTH Penjenjangan</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap">
              <input class="form-control" type="text" name="lth_penjenjangan" value="<?php echo $value['lth_penjenjangan']; ?>">
            </td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">Tanggal Capeg</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap">
              <?php if ($this->session->userdata('hak') == 1): ?>
                <input class="form-control" type="date" name="tgl_capeg" value="<?php echo $value['tgl_capeg']; ?>">
              <?php else: ?>
                <input class="form-control" type="date" name="tgl_capeg" value="<?php echo $value['tgl_capeg']; ?>" readonly>
              <?php endif; ?>
            </td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">MK Keseluruhan Tahun</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap">
              <?php if ($this->session->userdata('hak') == 1): ?>
                <input class="form-control" type="text" name="mk_kesel_thn" value="<?php echo $value['mk_kesel_thn'];?>">
              <?php else: ?>
                <input class="form-control" type="text" name="mk_kesel_thn" value="<?php echo $value['mk_kesel_thn'];?>" readonly>
              <?php endif; ?>
            </td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">MK Keseluruhan Bulan</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap">
              <?php if ($this->session->userdata('hak') == 1): ?>
                <input class="form-control" type="text" name="mk_kesel_bln" value="<?php echo $value['mk_kesel_bln'];?>">
              <?php else: ?>
                <input class="form-control" type="text" name="mk_kesel_bln" value="<?php echo $value['mk_kesel_bln'];?>" readonly>
              <?php endif; ?>
            </td>
          </tr>
          <?php if ($this->session->userdata('hak') == 1): ?>
            <tr>
              <td class="text-nowrap" style="width:5%;">Hak Akses</td>
              <td class="text-nowrap" style="width:1%;">:</td>
              <td class="text-nowrap">
                <select class="form-control" name="hak_akses">
                  <?php if ($value['hak_akses'] == 1): ?>
                    <option value="<?php echo $value['hak_akses'];?>" selected> Admin </option>
                    <option value="2">Pegawai</option>
                    <option value="3">Pemimpin</option>
                  <?php elseif($value['hak_akses'] == 2): ?>
                    <option value="<?php echo $value['hak_akses'];?>" selected> Pegawai </option>
                    <option value="1">Admin</option>
                    <option value="3">Pemimpin</option>
                  <?php else: ?>
                    <option value="<?php echo $value['hak_akses'];?>" selected> Pemimpin </option>
                    <option value="1">Admin</option>
                    <option value="2">Pegawai</option>
                  <?php endif; ?>
                </select>
              </td>
            </tr>
          <?php else: ?>
            <tr>
              <td class="text-nowrap" style="width:5%;">Hak Akses</td>
              <td class="text-nowrap" style="width:1%;">:</td>
              <td class="text-nowrap">
                <select class="form-control" name="hak_akses">
                  <option value="<?php echo $value['hak_akses']; ?>">
                    <?php 
                      switch ($value['hak_akses']) {
                        case '1':
                          echo "Admin";
                          break;
                        case '2':
                          echo "Pegawai";
                          break;
                        case '3':
                          echo "Pemimpin";
                          break;
                      }
                     ?>
                  </option>
                </select>
              </td>
            </tr>
          <?php endif; ?>
          <tr>
            <td class="text-nowrap" style="width:5%;">Email</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap">
              <input class="form-control" type="email" name="email" value="<?php echo $value['email']; ?>">
            </td>
          </tr>
          <tr>
            <td class="text-nowrap" style="width:5%;">Password</td>
            <td class="text-nowrap" style="width:1%;">:</td>
            <td class="text-nowrap">
              <input class="form-control" type="password" name="password" value="<?php echo $value['password']; ?>">
            </td>
          </tr>
          <tr>
            <td> </td>
            <td> </td>
            <td>
              <input type="submit" name="update" value="Update" class="btn btn-primary pull-right" style="margin-left:10px;">
              <?php if ($this->session->userdata('hak') == 1): ?>
                <a class="btn btn-warning pull-right" href="<?php echo site_url('admin/user');?>">Kembali</a>
              <?php else: ?>
                <a class="btn btn-warning pull-right" href="<?php echo site_url('admin/index');?>">Kembali</a>
              <?php endif; ?>
            </td>
          </tr>
        </table>  
      </div>
    <?php endforeach; ?>
  </form>
</div>
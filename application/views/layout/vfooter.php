</div><!-- /#right-panel -->

<!-- Right Panel -->

<script src="<?php echo site_url(); ?>assets/js/vendor/jquery-2.1.4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
<script src="<?php echo site_url(); ?>assets/js/plugins.js"></script>
<script src="<?php echo site_url(); ?>assets/js/main.js"></script>


<script src="<?php echo site_url(); ?>assets/js/lib/chart-js/Chart.bundle.js"></script>
<script src="<?php echo site_url(); ?>assets/js/dashboard.js"></script>
<script src="<?php echo site_url(); ?>assets/js/widgets.js"></script>
<script src="<?php echo site_url(); ?>assets/js/lib/vector-map/jquery.vmap.js"></script>
<script src="<?php echo site_url(); ?>assets/js/lib/vector-map/jquery.vmap.min.js"></script>
<script src="<?php echo site_url(); ?>assets/js/lib/vector-map/jquery.vmap.sampledata.js"></script>
<script src="<?php echo site_url(); ?>assets/js/lib/vector-map/country/jquery.vmap.world.js"></script>
<script src="<?php echo site_url(); ?>assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
<script src="<?php echo site_url(); ?>assets/js/lib/data-table/datatables.min.js"></script>
<script src="<?php echo site_url(); ?>assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
<script src="<?php echo site_url(); ?>assets/js/lib/data-table/dataTables.buttons.min.js"></script>
<script src="<?php echo site_url(); ?>assets/js/lib/data-table/jquery.dataTables.min.js"></script>

<script>
$(document).ready(function() {
  var cuti = $('#tblcuti').DataTable( {
      responsive: true
  } );
  
  var pengajuan = $('#tblpengajuan').DataTable( {
      responsive: true
  } );
  
  $('#close').on('click', function(){
    $('#form_cuti').hide();
  });
  
  $('#ajukan_cuti').on('click', function(){
    $('#form_cuti').show();
  });
  
  $('#tanggal_akhir_cuti').on('change',function(){
      oneDay = 24*60*60*1000;
      first = new Date($('#tanggal_awal_cuti').val());
      second = new Date($('#tanggal_akhir_cuti').val());
      diffDays = Math.round(Math.round((second.getTime() - first.getTime()) / (oneDay)));
      $("#jumlah_hari").val(diffDays+1);
  });
} );
</script>
</body>
</html>
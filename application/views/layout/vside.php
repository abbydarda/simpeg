<!-- Left Panel -->
<aside id="left-panel" class="left-panel">
<nav class="navbar navbar-expand-sm navbar-default">

    <div class="navbar-header">
        <a class="navbar-brand" href="./">Simpeg</a>
        <!-- <a class="navbar-brand hidden" href="./"><img src="images/logo2.png" alt="Logo"></a> -->
    </div>

    <div id="main-menu" class="main-menu collapse navbar-collapse">
        <ul class="nav navbar-nav">
            <li class="active">
                <a href="<?php echo base_url(); ?>admin"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
            </li>
            <h3 class="menu-title">Menu</h3><!-- /.menu-title -->
            
            <?php if ($this->session->userdata('hak') != 1): ?>
              <li style="display:none;">
                <a href="<?php echo base_url();?>admin/user"><i class="menu-icon fa fa-puzzle-piece"></i>Data Pegawai</a>
              </li>
              <li style="display:none;">
                <a href="<?php echo base_url();?>admin/data_cuti"><i class="menu-icon fa fa-puzzle-piece"></i>Data Cuti</a>
              </li>
            <?php else: ?>
              <li>
                <a href="<?php echo base_url();?>admin/user"><i class="menu-icon fa fa-puzzle-piece"></i>Data Pegawai</a>
              </li>
              <li>
                <a href="<?php echo base_url();?>admin/data_cuti"><i class="menu-icon fa fa-puzzle-piece"></i>Data Cuti</a>
              </li>
            <?php endif; ?>
              <li>
                <a href="<?php echo base_url();?>admin/pengajuan_cuti"><i class="menu-icon fa fa-puzzle-piece"></i> Pengajuan Cuti</a>
              </li>
              <li>
                <a class="nav-link" href="<?php echo base_url();?>admin/profil/<?php echo $this->session->userdata('id'); ?>"><i class="menu-icon fa fa-puzzle-piece"></i>Profil</a>
              </li>
              <li>
                <a class="nav-link" href="<?php echo base_url();?>main/logout"><i class="menu-icon fa fa-puzzle-piece"></i>Logout</a>
              </li>
        </ul>
    </div><!-- /.navbar-collapse -->
</nav>
</aside><!-- /#left-panel -->

<!-- Left Panel -->

<!-- Right Panel -->

<div id="right-panel" class="right-panel">

<!-- Header-->
<header id="header" class="header">

    <div class="header-menu">

        <div class="col-sm-1">
          <img src="<?php echo site_url(); ?>assets/images/jabar.png" height="50px;" width="50px;" alt="">
        </div>
        
        <div class="col-sm-6 align-middle">
          DINAS TANAMAN PANGAN DAN HORTIKULTURA <br> JAWA BARAT
        </div>

        <div class="col-sm-5">
            <div class="user-area dropdown float-right">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <!-- <img class="user-avatar rounded-circle" src="<?php echo site_url(); ?>assets/images/admin.jpg" alt="User Avatar"> -->
                </a>

                <div class="user-menu dropdown-menu">
                  <!-- <a class="nav-link" href="<?php echo base_url();?>admin/profil/<?php echo $this->session->userdata('id'); ?>"><i class="fa fa-power -off"></i>Profil</a> -->
                        <!-- <a class="nav-link" href="<?php echo base_url();?>main/logout"><i class="fa fa-power -off"></i>Logout</a> -->
                </div>
            </div>
        </div>
    </div>

</header><!-- /header -->
<!-- Header-->

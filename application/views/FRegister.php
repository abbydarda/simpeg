<!DOCTYPE html>
<html lang="en">
<head>
	<title>SIGN UP</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?php echo site_url(); ?>assets/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>assets/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>assets/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>assets/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>assets/css/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>assets/css/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>assets/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>assets/css/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>assets/css/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>assets/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>assets/css/main.css">
<!--===============================================================================================-->
</head>
<body>
	<div class="limiter">
		<div class="container-login100" style="background-image: url('assets/images/bg-01.jpg');">
			<div class="wrap-login100">
				<form class="login100-form validate-form" action="<?php echo base_url();?>main/register" method="post">
					<span class="login100-form-logo">
						<i class="zmdi zmdi-landscape"></i>
					</span>
					<span class="login100-form-title p-b-34 p-t-27">
						SIGN UP
					</span>
					<?php if (validation_errors()): ?>
						<div class="alert alert-danger">
							<?php
							 echo validation_errors(); 
							 ?>
						</div>
					<?php endif; ?>
					          
          <div class="row">
            <div class="col-md-6">
              <div class="wrap-input100 validate-input">
                <input class="input100" type="text" name="nip" placeholder="NIP" value="<?php echo set_value('nip'); ?>">
                <span class="focus-input100" data-placeholder="&#xf203;"></span>
              </div>
              
              <div class="wrap-input100 validate-input">
                <input class="input100" type="text" name="nama" placeholder="Nama" value="<?php echo set_value('nama'); ?>">
                <span class="focus-input100" data-placeholder="&#xf207;"></span>
              </div>
              
              <div class="wrap-input100 validate-input">
                <input class="input101" type="text" name="tempat_lahir" placeholder="Tempat Lahir" value="<?php echo set_value('tempat_lahir'); ?>">
                <span class="focus-input100" data-placeholder="&#xf175;"></span>
              </div>
            
              <div class="wrap-input100 validate-input">
                <input class="input100" type="date" name="tanggal_lahir" placeholder="Tanggal Lahir" value="<?php echo set_value('tanggal_lahir'); ?>">
                <span class="focus-input100" data-placeholder="&#xf332;"></span>
              </div>
							
							<div class="wrap-input100 validate-input">
								<input class="input100" type="text" name="email" placeholder="Email" value="<?php echo set_value('email'); ?>">
								<span class="focus-input100" data-placeholder="&#xf15a;"></span>
							</div>
            </div>
						
            <div class="col-md-6">
              <div class="wrap-input100 validate-input">
								<select class="form-control" name="jk" style="margin-left: 25px; margin-top: 7px; border:none; background-color: transparent; color:white; -webkit-appearance: none; -moz-appearance: none; appearance: none;">
                  <option value="" style="color:black;">Pilih Jenis Kelamin</option>
                  <option value="1" <?php echo set_select('jk', 1, False); ?> style="color:black;">Laki-Laki</option>
                  <option value="0" <?php echo set_select('jk', 0, False); ?> style="color:black;">Perempuan</option>
                </select>
								<span class="focus-input100" data-placeholder="&#xf211;"></span>
              </div>
              
							<div class="wrap-input100 validate-input">
								<select class="form-control" name="status" style="margin-left: 25px; margin-top: 7px; border:none; background-color: transparent; color:white; -webkit-appearance: none; -moz-appearance: none; appearance: none;">
                  <option value="" style="color:black;">Pilih Status Keluarga</option>
                  <option value="1" <?php echo set_select('status', 0, False); ?> style="color:black;">Belum Nikah</option>
                  <option value="2" <?php echo set_select('status', 1, False); ?> style="color:black;">Nikah</option>
                </select>
								<span class="focus-input100" data-placeholder="&#xf20d;"></span>
              </div>
              
              <div class="wrap-input100 validate-input form-inline">
                <select class="form-control" name="agama" style="margin-left: 25px; border:none; background-color: transparent; color:white;   -webkit-appearance: none; -moz-appearance: none; appearance: none;">
                  <option value="" style="color:black;">Pilih Agama</option>
                  <option value="1" <?php echo set_select('agama', 1, False); ?> style="color:black;">Islam</option>
                  <option value="2" <?php echo set_select('agama', 2, False); ?> style="color:black;">Kristen Protestan</option>
                  <option value="3" <?php echo set_select('agama', 3, False); ?> style="color:black;">Katolik</option>
                  <option value="4" <?php echo set_select('agama', 4, False); ?> style="color:black;">Hindu</option>
                  <option value="5" <?php echo set_select('agama', 5, False); ?> style="color:black;">Buddha</option>
                  <option value="6" <?php echo set_select('agama', 6, False); ?> style="color:black;">Kong Hu Cu</option>
                </select>
								<span class="focus-input100" data-placeholder="&#xf152;"></span>
              </div>
              
              <div class="wrap-input100 validate-input">
                <input class="input100" type="password" name="password" placeholder="Password">
                <span class="focus-input100" data-placeholder="&#xf191;"></span>
              </div>
              
            </div>
          </div>

					<div class="container-login100-form-btn">
            <a href="<?php echo base_url();?>Main/login" class="login100-form-btn">
              SIGN IN
            </a>
						<button class="login100-form-btn">
							SIGN UP
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	

<!--===============================================================================================-->
	<script src="<?php echo site_url(); ?>assets/js/vendor/jquery-2.1.4.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo site_url(); ?>assets/jsvendor/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo site_url(); ?>assets/jsvendor/js/popper.js"></script>
	<script src="<?php echo site_url(); ?>assets/jsvendor/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo site_url(); ?>assets/jsvendor/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo site_url(); ?>assets/jsvendor/moment.min.js"></script>
	<script src="<?php echo site_url(); ?>assets/jsvendor/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo site_url(); ?>assets/jsvendor/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo site_url(); ?>assets/js/main.js"></script>

</body>
</html>
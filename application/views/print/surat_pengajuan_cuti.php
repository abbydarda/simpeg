<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title style="display:none;"></title>
    
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/css/normalize.css">
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/css/themify-icons.css">
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/css/lib/datatable/buttons.bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/css/lib/datatable/buttons.dataTables.min.css">
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/css/lib/datatable/dataTables.bootstrap.min.css">
    
    <style media="print">
      @media print {
        @page { 
          margin-top: 0px;
          margin-bottom: 0px;
        }
      }
    </style>
  </head>
  <body onload="printme()" style="line-height:15px;">
  <?php foreach ($pengajuan as $value): ?>
    <?php 
      switch ($value['dinas']) {
        case '1':
          $dinas = "Tanaman Pangan";
          break;
        case '2':
          $dinas = "Tanaman Hortikultura";
          break;
        case '3':
          $dinas = "Prasarana DAN Sarana";
          break;
        case '4':
          $dinas = "Penyuluhan";
          break;
      }
     ?>
    <div id="print_area">
      <div class="container" style="margin-top:50px;">
        <div class="page-header" style="border-bottom:2px solid;">
          <div class="row">
            <div class="col-md-2">
              <img src="<?php echo site_url();?>assets/images/jabar.png" alt="" height="100px;" width="100px;" style="padding:top:30px;">
            </div>
            <div class="col-md-10" style="margin-top:-80px;">
              <div class="text-center">
                <p style="font-size:14px; font-weight:bold; margin-bottom:5px;">PEMERINTAH DAERAH PROVINSI JAWA BARAT</p>
                <p style="font-weight:bold; margin-bottom:5px;">DINAS TANAMAN PANGAN DAN HORTIKULTURA</p>
                <p style="font-size: 12px; margin-bottom:5px;">Jalan Surapati Nomer 71 Telepon (022) 2503884 </p>
                <p style="font-size: 12px; margin-bottom:5px;">Faksimile (022) 2500713 Website www.distan.jabarprov.go.id Email distan@jabarprov.go.id </p>
                <p style="font-size: 12px; margin-bottom:5px;">Bandung - 40133</p>
                <p></p>
              </div>
            </div>
          </div>
          
        </div><br>
        
        <div class="row">
          <div class="col-md-12">
            <div class="text-center">
              <p style="margin-bottom:5px; font-weight:bold;">SURAT IZIN CUTI TAHUNAN</p>
              <p>Nomor : &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp  /   &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp  /Pegum</p>
            </div><br>
          </div>
          <p>Diberikan Izin Cuti Tahunan kepada Pegawai Negeri Sipil :</p>
          <table class="table">
            <tr class="text-nowrap">
              <td style="width:10px;">Nama</td>
              <td style="width:10px;">:</td>
              <td><?php echo $value['nama']; ?></td>
            </tr>
            <tr class="text-nowrap">
              <td style="width:10px;">NIP</td>
              <td style="width:10px;">:</td>
              <td><?php echo $value['nip']; ?></td>
            </tr>
            <tr class="text-nowrap">
              <td style="width:10px;">Pangkat/Gol Ruang</td>
              <td style="width:10px;">:</td>
              <td><?php echo $value['golongan']; ?></td>
            </tr>
            <tr class="text-nowrap">
              <td style="width:10px;">Jabatan</td>
              <td style="width:10px;">:</td>
              <td><?php echo $value['jabatan_struktural']; ?></td>
            </tr>
            <tr class="text-nowrap">
              <td style="width:10px;">Satuan Organisasi</td>
              <td style="width:10px;">:</td>
              <td><?php echo $value['tempat_tugas']; ?></td>
            </tr>
          </table> <br>
        </div>
        <div class="col-md-12">
        <p>Selama <?php echo $value['jumlah_hari']; ?> hari kerja terhitung mulai tanggal <?php echo date('d',strtotime($value['tanggal_awal'])); ?> s.d <?php echo date('d M Y',strtotime($value['tanggal_akhir']));?> dengan ketentuan sebagai berikut :</p>
        <p>a. Sebelum menjalankan izin cuti tahunan, agar menyerahkan pekerjaannya kepada atasan langsungnya atau kepada pejabat lain yang ditunjuk. </p>
        <p>b. Setelah menjalankan izin cuti tahunan, wajib melaporkan diri kepada atasan langsungnya dan bekerja kembali sebagaimana biasa.</p>
        <p>Demikian surat izin cuti tahunan ini dibuat untuk dapat digunanakan sebagaimana mestinya.</p> <br>
        <br> <br> <br> <br> <br> <br>
          <p class="text-center pull-right">Bandung, <?php echo date('d M Y',strtotime($value['tanggal_pengajuan']));?> <br>
          a.n KEPALA DINAS TANAMAN PANGAN DAN HORTIKULTURA<br>
          JAWA BARAT <br>
          Sekretaris <br> <br> <br> <br> <br>
          <b>IBRAHIM SYAF S.A.,S.Sos.,M.Si</b> <br>
          Pembina <br>
          NIP : 19601025 198603 1 004 <br> <br> <br> <br>
        </p>
      </div> <br> <br> </rb> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br>
        
        <div class="col-md-12">
          <p class="pull-right">
              Bandung, <?php echo date('d M Y',strtotime($value['tanggal_pengajuan']));?> <br>
              kepada <br>
              yth. Kepala Bidang <?php echo $dinas; ?> <br>
              melalui : <br>
              kepala seksi ketenagaan <br>
              di tempat
          </p> 
        </div> <br> <br> <br> <br> <br> <br> <br> <br>
        
        <div class="col-md-12">
          <p>
            yang bertanda tangan di bawah ini : <br>
          </p>
          <table class="table">
            <tr class="text-nowrap">
              <td style="width:10px;">Nama</td>
              <td style="width:10px;">:</td>
              <td><?php echo $value['nama']; ?></td>
            </tr>
            <tr class="text-nowrap">
              <td style="width:10px;">NIP</td>
              <td style="width:10px;">:</td>
              <td><?php echo $value['nip']; ?></td>
            </tr>
            <tr class="text-nowrap">
              <td style="width:10px;">Pangkat/Gol Ruang</td>
              <td style="width:10px;">:</td>
              <td><?php echo $value['golongan']; ?></td>
            </tr>
            <tr class="text-nowrap">
              <td style="width:10px;">Jabatan</td>
              <td style="width:10px;">:</td>
              <td><?php echo $value['jabatan_struktural']; ?></td>
            </tr>
            <tr class="text-nowrap">
              <td style="width:10px;">Satuan Organisasi</td>
              <td style="width:10px;">:</td>
              <td><?php echo $value['tempat_tugas']; ?></td>
            </tr>
          </table>
          <p>Dengan ini mengajukan permohonan / pengajuan izin cuti tahunan selama <?php echo $value['jumlah_hari']; ?> hari kerja. </p>
          <p>Terhitung mulai tanggal <?php echo date('d',strtotime($value['tanggal_awal'])); ?> s.d <?php echo date('d M Y',strtotime($value['tanggal_akhir']));?>  </p>
          <p>Selama menjalankan izin cuti tahunan saya berada di ______  </p>
          <p>Demikian permintaan ini saya buat untuk dapat dipertimbangkan sebagaimana mestinya.</p> <br>
          
          <p class="text-center pull-right">
            Hormat Saya, <br> <br> <br> <br>
            <?php echo $value['nama']; ?> <br>
            <?php echo $value['nip']; ?>
          </p> <br> <br>
          
          <table class="table">
            <thead class="text-center text-nowrap">
              <tr>
                <th>CATATAN PEJABAT KEPEGAWAIAN</th>
                <th>CATATAN PERTIMBANGAN ATASAN LANGSUNG</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  Cuti yang telah diambil dalam tahun yang bersangkutan: <br>
                  1. Cuti Tahunan : <?php echo $value['tahunan']; ?> <br> 
                  2. Cuti Besar : <?php echo $value['besar']; ?> <br> 
                  3. Cuti Sakit : <?php echo $value['sakit']; ?> <br> 
                  4. Cuti Bersalin : <?php echo $value['bersalin']; ?> <br> 
                  5. Cuti karena Alasan Penting : <?php echo $value['alasan']; ?> <br> 
                  6. Keterangan Lain-Lain : <?php echo $value['lain']; ?> <br> 
                </td>
                <td>
                  <p class="text-center">
                    Menyetujui <br> <br>
                    ATASAN LANGSUNG <br>
                    <?php echo $value['jabatan_pemimpin'] ?> <br> <br> <br> <br>
                    <b><?php echo $value['pemimpin']; ?></b> <br>
                    pembina <br>
                    NIP : <?php echo $value['nip_pemimpin'] ?> <br> <br> <br>
                    
                    KEPUTUSAN PEJABAT YANG BERWENANG <br>
                    MEMBERIKAN CUTI <br> <br> <br> <br>
                    <b><?php echo $value['pejabat']; ?></b> <br>
                    <?php echo $value['jabatan_pejabat'] ?> <br>
                    NIP : <?php echo $value['nip_pejabat'] ?>
                  </p>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  <?php endforeach; ?>
  </body>
  <script type="text/javascript">
        function printme() {
          var printContents = document.getElementById('print_area').innerHTML;
          
          document.body.innerHTML = printContents;
    
          window.print();
          window.close();
        }
  </script>
</html>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title style="display:none;"></title>
    
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/css/normalize.css">
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/css/themify-icons.css">
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/css/lib/datatable/buttons.bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/css/lib/datatable/buttons.dataTables.min.css">
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/css/lib/datatable/dataTables.bootstrap.min.css">
    
    <style media="print">
      @media print {
        @page { 
          margin-top: 0px;
          margin-bottom: 0px;
        }
      }
    </style>
  </head>
  <body onload="printme()" style="line-height:15px;">
    
    <div id="print_area">
      <div class="container-fluid" style="margin-top:50px;">
        <div class="table-responsive table-bordered">
          <table class="table" style="font-size:9px;">
            <thead class="text-nowrap" style="line-height:20px; height:25px; min-height:25px;">
              <tr>
                <th class="align-middle">NO</th>
                <th class="align-middle">NAMA <br> NIP <br> NO KARPEG <br> TEMPAT & TANGGAL LAHIR </th>
                <th class="align-middle">JENIS KELAMIN <br> STATUS KELUARGA <br> AGAMA</th>
                <th class="align-middle">GOLONGAN <br> TMT GOLONGAN <br> MK GOLONGAN <br> TH &nbsp&nbsp&nbsp&nbsp BL </th>
                <th class="align-middle">JABATAN STRUKTURAL <br> JABATAN FUNGSIONAL <br> TMT STRUKTURAL / TMT FUNGSIONAL <br> TEMPAT DINAS </th>
                <th class="align-middle">PENDIDIKAN AKHIR<br> JURUSAN</th>
                <th class="align-middle">LTH PENJENJANGAN</th>
                <th class="align-middle">TANGGAL CAPEG <br> MK KESEL TAHUN <br> MK KESEL BULAN </th>
              </tr>
            </thead>
            <tbody class="text-nowrap">
              <?php $no=1; foreach ($pegawai as $value): ?>
                <?php 
                  $jk = ($value['jk'] == 0 ) ? "Perempuan" : "Laki-Laki";
                  $status_keluarga = ($value['status_keluarga'] == 1 ) ? "Belum Nikah" : "Nikah";
                  switch ($value['agama']) {
                    case '1':
                      $agama = "Islam";
                      break;
                    case '2':
                      $agama = "Kristen Protestan";
                      break;
                    case '3':
                      $agama = "Katolik";
                      break;
                    case '4':
                      $agama = "Hindu";
                      break;
                    case '5':
                      $agama = "Buddha";
                      break;
                    case '6':
                      $agama = "Kong Hu Cu*";
                      break;
                  }
                 ?>
              <tr style="line-height:20px; height:25px; min-height:25px;">
                <td width="1px" class="text-nowrap"><?php echo $no++; ?></td>
                <td width="1px">
                  <?php echo $value['nama']."<br>".$value['nip']." / ".$value['no_karpeg']."<br>".$value['tempat_lahir'].", ".date('d-m-Y',strtotime($value['tanggal_lahir'])) ?>
                </td>
                <td width="1px;">
                  <?php echo $jk."<br>".$status_keluarga."<br>".$agama ?>
                </td>
                <td width="1px;">
                  <?php echo $value['golongan']."<br>".date('d-m-Y',strtotime($value['tmt_golongan']))."<br>".$value['mk_golongan_thn']."&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp".$value['mk_golongan_bulan'] ?>
                </td>
                <td width="1px;">
                  <?php echo $value['jabatan_struktural']."<br>".$value['jabatan_fungsional']."<br>".$value['tmt_jabatan']." <br> ".$value['tempat_tugas'] ?>
                </td>
                <td width="1px;">
                  <?php echo $value['pendidikan_akhir']."<br>".$value['jurusan'] ?>
                </td>
                <td width="1px;">
                  <?php echo $value['lth_penjenjangan'] ?>
                </td>
                <td width="1px;" class="text-center">
                  <?php echo $value['tgl_capeg']."<br>".$value['mk_kesel_thn']."<br>".$value['mk_kesel_bln'] ?>
                </td>
              </tr>
            <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </body>
  <script type="text/javascript">
        function printme() {
          var printContents = document.getElementById('print_area').innerHTML;
          
          document.body.innerHTML = printContents;
          
          window.print();
          window.close();
        }
  </script>
</html>
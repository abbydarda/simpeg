<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	 function __construct()
	 {
	 	parent::__construct();
		$this->load->model(array('login_m','admin_m'));
	 }
	 
	public function index()
	{
		$this->load->view('vlogin');
	}
	
	public function signup()
	{
		$this->load->view('FRegister');
	}
	
	public function register()
	{
		$this->form_validation->set_rules('nip', 'nip', 'required|exact_length[18]|numeric');
		$this->form_validation->set_rules('nama', 'nama', 'required');
		$this->form_validation->set_rules('tempat_lahir', 'tempat lahir', 'required');
		$this->form_validation->set_rules('tanggal_lahir', 'tanggal lahir', 'required');
		$this->form_validation->set_rules('email', 'email', 'required|is_unique[user.email]');
		$this->form_validation->set_rules('status', 'status', 'required');
		$this->form_validation->set_rules('agama', 'agama', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|max_length[25]');
		
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('FRegister');
		}else {
			$req = $this->input->post();
			
			$nip = (isset($req['nip'])) ? $req['nip'] : null ;
			$nama = (isset($req['nama'])) ? $req['nama'] : null ;
			$nama = (isset($req['nama'])) ? $req['nama'] : null ;
			$tempat_lahir = (isset($req['tempat_lahir'])) ? $req['tempat_lahir'] : null ;
			$tanggal_lahir = (isset($req['tanggal_lahir'])) ? $req['tanggal_lahir'] : null ;
			$jk = (isset($req['jk'])) ? $req['jk'] : null ;
			$status_keluarga = (isset($req['status'])) ? $req['status'] : null ;
			$agama = (isset($req['agama'])) ? $req['agama'] : null ;
			$no_karpeg = (isset($req['no_karpeg'])) ? $req['no_karpeg'] : null ;
			$golongan = (isset($req['golongan'])) ? $req['golongan'] : null ;
			$tmt_golongan = (isset($req['tmt_golongan'])) ? $req['tmt_golongan'] : null ;
			$jabatan_struktural = (isset($req['jabatan_struktural'])) ? $req['jabatan_struktural'] : null ;
			$jabatan_fungsional = (isset($req['jabatan_fungsional'])) ? $req['jabatan_fungsional'] : null ;
			$tmt_jabatan = (isset($req['tmt_jabatan'])) ? $req['tmt_jabatan'] : null ;
			$tempat_tugas = (isset($req['tempat_tugas'])) ? $req['tempat_tugas'] : null ;
			$pendidikan_akhir = (isset($req['pendidikan_akhir'])) ? $req['pendidikan_akhir'] : null ;
			$jurusan = (isset($req['jurusan'])) ? $req['jurusan'] : null ;
			$lth_penjenjangan = (isset($req['lth_penjenjangan'])) ? $req['lth_penjenjangan'] : null ;
			$tgl_capeg = (isset($req['tgl_capeg'])) ? $req['tgl_capeg'] : null ;
			$mk_kesel_bln = (isset($req['mk_kesel_bln'])) ? $req['mk_kesel_bln'] : null ;
			$mk_kesel_thn = (isset($req['mk_kesel_thn'])) ? $req['mk_kesel_thn'] : null ;
			$mk_golongan_bln = (isset($req['mk_golongan_bln'])) ? $req['mk_golongan_bln'] : null ;
			$mk_golongan_thn = (isset($req['mk_golongan_thn'])) ? $req['mk_golongan_thn'] : null ;
			$hak_akses = (isset($req['hak_akses'])) ? $req['hak_akses'] : null ;
			$email = (isset($req['email'])) ? $req['email'] : null ;
			$password = (isset($req['password'])) ? $req['password'] : null ;
			
			$payload = [
				'nip' => $nip,
				'nama' => $nama,
				'no_karpeg' => $no_karpeg,
				'tempat_lahir' => $tempat_lahir,
				'tanggal_lahir' => $tanggal_lahir,
				'jk' => $jk,
				'status_keluarga' => $status_keluarga,
				'agama' => $agama,
				'golongan' => $golongan,
				'tmt_golongan' => $tmt_golongan,
				'jabatan_struktural' => $jabatan_struktural,
				'jabatan_fungsional' => $jabatan_fungsional,
				'tmt_jabatan' => $tmt_jabatan,
				'tempat_tugas' => $tempat_tugas,
				'pendidikan_akhir' => $pendidikan_akhir,
				'jurusan' => $jurusan,
				'lth_penjenjangan' => $lth_penjenjangan,
				'tgl_capeg' => $tgl_capeg,
				'mk_kesel_thn' => $mk_kesel_thn,
				'mk_kesel_bln' => $mk_kesel_bln,
				'mk_golongan_thn' => $mk_golongan_thn,
				'mk_golongan_bulan' => $mk_golongan_bln,
				'hak_akses' => 2,
				'email' => $email,
				'password' => $password
			];
						
			$res = $this->admin_m->insert_user($payload);
			
			if ($res > 0) {
				$this->session->set_flashdata('msg','Pendaftaran Berhasil, Silahkan Login');
				redirect('main/login');
			}else {
				redirect('main/register');
			}
		}
	}
	
	public function login()
	{
		
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('vlogin');
		}else {
			$req = $this->input->post();
			
			$check = $this->login_m->check_login($req['username'],$req['password'])[0];
			if(count($check) == 0) {
				$this->session->set_flashdata('err','username atau password salah');
				redirect('main/login');
			}else{
				$data_session=array('id' => $check['id'],'nama' => $check['nama'],'hak' => $check['hak_akses'],'jk' => $check['jk'],'status' => 'login');
				$this->session->set_userdata($data_session);
				redirect('admin');
			}
		}
		
	}
	
	public function logout()
	{
		$this->session->unset_userdata('id');
		$this->session->unset_userdata('nama');
		$this->session->unset_userdata('hak');
		$this->session->unset_userdata('jk');
		$this->session->unset_userdata('status');
		$this->session->sess_destroy();
		redirect('main');
	}	
}
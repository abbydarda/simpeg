<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 
 */
class Admin extends CI_Controller
{
  
  function __construct()
  {
    parent::__construct();
    $this->load->model(array('login_m','admin_m'));
  }
  
  public function index()
  {
    $data['title'] = "Dashboard";
    $this->load->view('layout/vheader',$data);
    $this->load->view('layout/vside');
    $this->load->view('admin/dashboard',$data);
    $this->load->view('layout/vfooter');
  }
  
  public function user()
  {
    $data['title'] = "Data Pegawai";
    $data['user'] = $this->login_m->get_user();
    $this->load->view('layout/vheader',$data);
    $this->load->view('layout/vside');
    $this->load->view('admin/pegawai',$data);
    $this->load->view('layout/vfooter');
  }
  
  public function create()
  {
    $this->form_validation->set_rules('nip', 'nip', 'required|exact_length[18]|numeric');
		$this->form_validation->set_rules('nama', 'nama', 'required');
		$this->form_validation->set_rules('tempat_lahir', 'tempat lahir', 'required');
		$this->form_validation->set_rules('tanggal_lahir', 'tanggal lahir', 'required');
		$this->form_validation->set_rules('email', 'email', 'required|is_unique[user.email]');
		$this->form_validation->set_rules('status', 'status', 'required');
		$this->form_validation->set_rules('agama', 'agama', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|max_length[25]');
		
		if ($this->form_validation->run() == FALSE) {
      $data['title'] = "Form Edit Pegawai";
      
      $this->load->view('layout/vheader',$data);
      $this->load->view('layout/vside');
      $this->load->view('admin/admin_create_pegawai');
      $this->load->view('layout/vfooter');
		}else {
			$req = $this->input->post();
			
			$nip = (isset($req['nip'])) ? $req['nip'] : null ;
			$nama = (isset($req['nama'])) ? $req['nama'] : null ;
			$nama = (isset($req['nama'])) ? $req['nama'] : null ;
			$tempat_lahir = (isset($req['tempat_lahir'])) ? $req['tempat_lahir'] : null ;
			$tanggal_lahir = (isset($req['tanggal_lahir'])) ? $req['tanggal_lahir'] : null ;
			$jk = (isset($req['jk'])) ? $req['jk'] : null ;
			$status_keluarga = (isset($req['status'])) ? $req['status'] : null ;
			$agama = (isset($req['agama'])) ? $req['agama'] : null ;
			$no_karpeg = (isset($req['no_karpeg'])) ? $req['no_karpeg'] : null ;
			$golongan = (isset($req['golongan'])) ? $req['golongan'] : null ;
			$tmt_golongan = (isset($req['tmt_golongan'])) ? $req['tmt_golongan'] : null ;
			$jabatan_struktural = (isset($req['jabatan_struktural'])) ? $req['jabatan_struktural'] : null ;
			$jabatan_fungsional = (isset($req['jabatan_fungsional'])) ? $req['jabatan_fungsional'] : null ;
			$tmt_jabatan = (isset($req['tmt_jabatan'])) ? $req['tmt_jabatan'] : null ;
			$tempat_tugas = (isset($req['tempat_tugas'])) ? $req['tempat_tugas'] : null ;
			$pendidikan_akhir = (isset($req['pendidikan_akhir'])) ? $req['pendidikan_akhir'] : null ;
			$jurusan = (isset($req['jurusan'])) ? $req['jurusan'] : null ;
			$lth_penjenjangan = (isset($req['lth_penjenjangan'])) ? $req['lth_penjenjangan'] : null ;
			$tgl_capeg = (isset($req['tgl_capeg'])) ? $req['tgl_capeg'] : null ;
			$mk_kesel_bln = (isset($req['mk_kesel_bln'])) ? $req['mk_kesel_bln'] : null ;
			$mk_kesel_thn = (isset($req['mk_kesel_thn'])) ? $req['mk_kesel_thn'] : null ;
			$mk_golongan_bln = (isset($req['mk_golongan_bulan'])) ? $req['mk_golongan_bulan'] : null ;
			$mk_golongan_thn = (isset($req['mk_golongan_thn'])) ? $req['mk_golongan_thn'] : null ;
			$hak_akses = (isset($req['hak_akses'])) ? $req['hak_akses'] : null ;
			$email = (isset($req['email'])) ? $req['email'] : null ;
			$password = (isset($req['password'])) ? $req['password'] : null ;
      
      if (!empty($_FILES['userfile']['name'])) {
        $lokasi_file = $_FILES['userfile']['tmp_name'];
        $tipe_file   = $_FILES['userfile']['type'];
        $nama_file   = md5(time().$_FILES['userfile']['name']);
        $direktori   = "assets/images/pegawai/$nama_file";
        
        if (substr($tipe_file,0,5) == "image") {
          if (move_uploaded_file($lokasi_file,$direktori)){
            $foto = $direktori;
          }else{
            $foto = '';
          }
        }else {
          $this->session->set_flashdata('err','Harap Upload Gambar dengan Ekstensi jpg|png|jpeg');
          redirect("admin/create/");
        }
      }else {
          $foto = "";
      }
			
			$payload = [
				'nip' => $nip,
				'nama' => $nama,
				'no_karpeg' => $no_karpeg,
				'tempat_lahir' => $tempat_lahir,
				'tanggal_lahir' => $tanggal_lahir,
				'jk' => $jk,
				'status_keluarga' => $status_keluarga,
				'agama' => $agama,
				'golongan' => $golongan,
				'tmt_golongan' => $tmt_golongan,
				'jabatan_struktural' => $jabatan_struktural,
				'jabatan_fungsional' => $jabatan_fungsional,
				'tmt_jabatan' => $tmt_jabatan,
				'tempat_tugas' => $tempat_tugas,
				'pendidikan_akhir' => $pendidikan_akhir,
				'jurusan' => $jurusan,
				'lth_penjenjangan' => $lth_penjenjangan,
				'tgl_capeg' => $tgl_capeg,
				'mk_kesel_thn' => $mk_kesel_thn,
				'mk_kesel_bln' => $mk_kesel_bln,
				'mk_golongan_thn' => $mk_golongan_thn,
				'mk_golongan_bulan' => $mk_golongan_bln,
				'hak_akses' => $hak_akses,
        'email' => $email,
				'foto' => $foto,
				'password' => $password
			];
						
			$res = $this->admin_m->insert_user($payload);
			
			if ($res > 0) {
				$this->session->set_flashdata('msg','Data Berhasil Ditambahkan');
				redirect('admin/user');
			}else {
        $this->session->set_flashdata('err','Data Gagal Ditambahkan');
				redirect('admin/user');
			}
		}
  }
  
  public function edit($id)
  {
    $data['title'] = "Form Edit Pegawai";
    $data['user'] = $this->login_m->get_user_id($id);
    
    $this->load->view('layout/vheader',$data);
    $this->load->view('layout/vside');
    $this->load->view('admin/admin_edit_pegawai',$data);
    $this->load->view('layout/vfooter');
  }
  
  public function update_pegawai($id)
  {
    
    $this->form_validation->set_rules('nip', 'nip', 'required|exact_length[18]|numeric');
    $this->form_validation->set_rules('nama', 'nama', 'required');
    $this->form_validation->set_rules('tempat_lahir', 'tempat lahir', 'required');
    $this->form_validation->set_rules('tanggal_lahir', 'tanggal lahir', 'required');
    $this->form_validation->set_rules('email', 'email', 'required');
    $this->form_validation->set_rules('status', 'status', 'required');
    $this->form_validation->set_rules('agama', 'agama', 'required');
    $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|max_length[25]');
    
    if ($this->form_validation->run() == FALSE) {
      $data['title'] = "Form Edit Pegawai";
      $data['user'] = $this->login_m->get_user_id($id);
      
      $this->load->view('layout/vheader',$data);
      $this->load->view('layout/vside');
      $this->load->view('admin/admin_edit_pegawai',$data);
      $this->load->view('layout/vfooter');
    }else {
      $req = $this->input->post();
      
      $nip = (isset($req['nip'])) ? $req['nip'] : null ;
      $nama = (isset($req['nama'])) ? $req['nama'] : null ;
      $nama = (isset($req['nama'])) ? $req['nama'] : null ;
      $tempat_lahir = (isset($req['tempat_lahir'])) ? $req['tempat_lahir'] : null ;
      $tanggal_lahir = (isset($req['tanggal_lahir'])) ? $req['tanggal_lahir'] : null ;
      $jk = (isset($req['jk'])) ? $req['jk'] : null ;
      $status_keluarga = (isset($req['status'])) ? $req['status'] : null ;
      $agama = (isset($req['agama'])) ? $req['agama'] : null ;
      $no_karpeg = (isset($req['no_karpeg'])) ? $req['no_karpeg'] : null ;
      $golongan = (isset($req['golongan'])) ? $req['golongan'] : null ;
      $tmt_golongan = (isset($req['tmt_golongan'])) ? $req['tmt_golongan'] : null ;
      $jabatan_struktural = (isset($req['jabatan_struktural'])) ? $req['jabatan_struktural'] : null ;
      $jabatan_fungsional = (isset($req['jabatan_fungsional'])) ? $req['jabatan_fungsional'] : null ;
      $tmt_jabatan = (isset($req['tmt_jabatan'])) ? $req['tmt_jabatan'] : null ;
      $tempat_tugas = (isset($req['tempat_tugas'])) ? $req['tempat_tugas'] : null ;
      $pendidikan_akhir = (isset($req['pendidikan_akhir'])) ? $req['pendidikan_akhir'] : null ;
      $jurusan = (isset($req['jurusan'])) ? $req['jurusan'] : null ;
      $lth_penjenjangan = (isset($req['lth_penjenjangan'])) ? $req['lth_penjenjangan'] : null ;
      $tgl_capeg = (isset($req['tgl_capeg'])) ? $req['tgl_capeg'] : null ;
      $mk_kesel_bln = (isset($req['mk_kesel_bln'])) ? $req['mk_kesel_bln'] : null ;
      $mk_kesel_thn = (isset($req['mk_kesel_thn'])) ? $req['mk_kesel_thn'] : null ;
      $mk_golongan_bln = (isset($req['mk_golongan_bulan'])) ? $req['mk_golongan_bulan'] : null ;
      $mk_golongan_thn = (isset($req['mk_golongan_thn'])) ? $req['mk_golongan_thn'] : null ;
      $hak_akses = (isset($req['hak_akses'])) ? $req['hak_akses'] : null ;
      $email = (isset($req['email'])) ? $req['email'] : null ;
      $password = (isset($req['password'])) ? $req['password'] : null ;
      
      if (!empty($_FILES['userfile']['name'])) {
        $lokasi_file = $_FILES['userfile']['tmp_name'];
    	  $tipe_file   = $_FILES['userfile']['type'];
    		$nama_file   = md5(time().$_FILES['userfile']['name']);
    		$direktori   = "assets/images/pegawai/$nama_file";
        
        if (substr($tipe_file,0,5) == "image") {
          if (move_uploaded_file($lokasi_file,$direktori)){
            $foto = $direktori;
          }else{
            $foto = '';
          }
        }else {
          $this->session->set_flashdata('err','Harap Upload Gambar dengan Ekstensi jpg|png|jpeg');
          redirect("admin/edit/$id");
        }
      }else {
        if ($req['foto'] != "") {
          $foto = $req['foto'];
          // var_dump($req['foto']);die();
        }else {
          $foto = "";
        }
      }
      
      
      $payload = [
        'nip' => $nip,
        'nama' => $nama,
        'no_karpeg' => $no_karpeg,
        'tempat_lahir' => $tempat_lahir,
        'tanggal_lahir' => $tanggal_lahir,
        'jk' => $jk,
        'status_keluarga' => $status_keluarga,
        'agama' => $agama,
        'golongan' => $golongan,
        'tmt_golongan' => $tmt_golongan,
        'jabatan_struktural' => $jabatan_struktural,
        'jabatan_fungsional' => $jabatan_fungsional,
        'tmt_jabatan' => $tmt_jabatan,
        'tempat_tugas' => $tempat_tugas,
        'pendidikan_akhir' => $pendidikan_akhir,
        'jurusan' => $jurusan,
        'lth_penjenjangan' => $lth_penjenjangan,
        'tgl_capeg' => $tgl_capeg,
        'mk_kesel_thn' => $mk_kesel_thn,
        'mk_kesel_bln' => $mk_kesel_bln,
        'mk_golongan_thn' => $mk_golongan_thn,
        'mk_golongan_bulan' => $mk_golongan_bln,
        'hak_akses' => $hak_akses,
        'email' => $email,
        'foto' => $foto,
        'password' => $password
      ];
      
      $res = $this->admin_m->update_user($payload,$id);
      
      if ($res > 0) {
        if ($this->session->userdata('hak') == 1) {
          $this->session->set_flashdata('msg','Data Berhasil Diubah');
          redirect('admin/user');
        }else {
          $this->session->set_flashdata('msg','Data Berhasil Diubah');
          redirect('admin/edit/$id');
        }
      }else {
        if ($this->session->userdata('hak') == 1) {
          $this->session->set_flashdata('err','Tidak Ada Data yang Diubah');
          redirect("admin/user");
        }else {
          $this->session->set_flashdata('err','Tidak Ada Data yang Diubah');
          redirect("admin/edit/$id");
        }
      }
    }
  }
  
  public function delete($id)
  {
    $res = $this->admin_m->delete_user($id);
    
    if ($res > 0) {
      $this->session->set_flashdata('msg','Data Berhasil Dihapus');
      redirect('admin/user');
    }else {
      $this->session->set_flashdata('err','Data Gagal Dihapus');
      redirect("admin/user");
    }
  }
  
  public function profil($id)
  {
    $data['title'] = "Profil Pegawai";
    $data['user'] = $this->login_m->get_user_id($id);
    
    $this->load->view('layout/vheader',$data);
    $this->load->view('layout/vside');
    $this->load->view('admin/profil_pegawai',$data);
    $this->load->view('layout/vfooter');
  }
  
  public function data_cuti()
  {
    $tahun = date('Y');
    $data['title'] = "Data Cuti";
    $data['user'] = $this->login_m->get_user();
    $data['cuti'] = $this->admin_m->get_cuti_user($tahun);
    
    $this->load->view('layout/vheader',$data);
    $this->load->view('layout/vside');
    $this->load->view('admin/data_cuti',$data);
    $this->load->view('layout/vfooter');
  }
  
  public function generate_cuti()
  {
    $tahun = date('Y');
    $get_user = $this->admin_m->get_diff_user($tahun);
    
    if (count($get_user > 0)) {
      foreach ($get_user as $user) {
        foreach (array_chunk($user,100) as $users) {
          $jml_bersalin = ($users['1'] == 0 && $users['2'] == 2) ? 40 : NULL ;
          
          $payload = [
            'id_user' => $users['0'],
            'jumlah_cuti_tahunan' => 12,
            'jumlah_cuti_bersalin' => $jml_bersalin,
            'tahun' => $tahun
          ];
          
          $res = $this->admin_m->insert_cuti($payload);
        }
      }
      if ($res > 0) {
        $this->session->set_flashdata('msg','Berhasil Generate Data Cuti');
        redirect('admin/data_cuti');
      }else {
        $this->session->set_flashdata('err','Gagal, Data cuti pegawai sudah lengkap');
        redirect('admin/data_cuti');
      }
    }
  }
  
  public function pengajuan_cuti()
  {
        $tahun = date('Y');
        $data['title'] = "Pengajuan Cuti";
        $data['sisa_cuti'] = $this->admin_m->get_sisa_cuti($this->session->userdata('id'));
        $data['pemimpin'] = $this->admin_m->get_pemimpin();
        $data['pejabat'] = $this->admin_m->get_pejabat($this->session->userdata('id'));
        
        if ($data['sisa_cuti'] == NULL) {
          $data['sisa_cuti'] = 0;
        }else {
          $data['sisa_cuti'] = $data['sisa_cuti'][0];
        }
        
        
        if ($this->session->userdata('hak') == 1 || $this->session->userdata('hak') == 3) {
          $data['pengajuan'] = $this->admin_m->get_data_pengajuan();
        }else {
          $data['pengajuan'] = $this->admin_m->get_data_pengajuan($this->session->userdata('id'));
        }
        $this->load->view('layout/vheader',$data);
        $this->load->view('layout/vside');
        $this->load->view('admin/pengajuan_cuti',$data);
        $this->load->view('layout/vfooter');
  }
  
  public function create_pengajuan_cuti($id)
  {
    
    $this->form_validation->set_rules('tanggal_pengajuan', 'tanggal_pengajuan', 'required');
    $this->form_validation->set_rules('jenis_cuti', 'Jenis Cuti', 'required');
    $this->form_validation->set_rules('tanggal_awal', 'Tanggal Awal', 'required');
    $this->form_validation->set_rules('tanggal_akhir', 'Tanggal Akhir', 'required');
    $this->form_validation->set_rules('jumlah_hari', 'Jumlah Hari', 'required');
    $this->form_validation->set_rules('id_pimpinan', 'Pemimpin', 'required');
    $this->form_validation->set_rules('id_pejabat', 'Pejabat', 'required');
    $this->form_validation->set_rules('dinas', 'Dinas', 'required');

    if ($this->form_validation->run() == FALSE) {
      $tahun = date('Y');
      $data['title'] = "Formulir Pengajuan Cuti";
      
      $this->load->view('layout/vheader',$data);
      $this->load->view('layout/vside');
      $this->load->view("admin/pengajuan_cuti");
      $this->load->view('layout/vfooter');
    }else {
      $req = $this->input->post();
      
      if ($req['jumlah_hari'] > $req['sisa_cuti']) {
        $this->session->set_flashdata('err','Maaf Sisa Cuti Anda Tidak Cukup');
        redirect('admin/pengajuan_cuti');
      }else {
        $payload_pengajuan = [
          'id_user' => $id,
          'id_pejabat' => $req['id_pejabat'],
          'id_pemimpin' => $req['id_pimpinan'],
          'tanggal_pengajuan' => date('Y-m-d'),
          'jenis_cuti' => $req['jenis_cuti'],
          'tanggal_awal' => $req['tanggal_awal'],
          'tanggal_akhir' => $req['tanggal_akhir'],
          'jumlah_hari' => $req['jumlah_hari'],
          'status_pengajuan' => 0,
          'dinas' => $req['dinas']
        ];
        
        $res = $this->admin_m->insert_pengajuan($payload_pengajuan);
        
        if ($res > 0) {
          $this->session->set_flashdata('msg','Berhasil Melakukan Pengajuan Cuti');
          redirect('admin/pengajuan_cuti');
        }else {
          $this->session->set_flashdata('err','Gagal Melakukan Pengajuan Cuti');
          redirect('admin/pengajuan_cuti');
        }
      }
      }
    
  }
  
  public function update_pengajuan($status, $id)
  {
    $tahun = date('Y');
    $data = $this->admin_m->get_data_pengajuan_id($id);
    
    foreach ($data as $value) {
      if ($value['jenis_cuti'] != "bersalin") {
        $jumlah = $value['jumlah_hari'];
      }else {
        $jumlah = 40;
      }
      
      $status = [
        'status_pengajuan' => $status
      ];
      
      $update_pengajuan = $this->admin_m->update_pengajuan($id,$status);
      $update_cuti = $this->admin_m->update_cuti($value['id_user'],$jumlah, $tahun);
      if ($update_pengajuan > 0 && $update_cuti > 0) {
        $this->session->set_flashdata('msg','Proses Berhasil');
        redirect('admin/pengajuan_cuti');
      }else {
        $this->session->set_flashdata('err','Proses Gagal');
        redirect('admin/pengajuan_cuti');
      }
    }
  }
  
  public function print_surat_pengajuan($id)
  {
    $data['pengajuan'] = $this->admin_m->get_data_pengajuan_by_id($id);
    // print_r($data);die();
    $this->load->view('print/surat_pengajuan_cuti',$data);
  }
  
  public function print_data_pegawai()
  {
    $data['pegawai'] = $this->login_m->get_user();
    // print_r($data);die();
    $this->load->view('print/data_pegawai',$data);
  }
  
}

 ?>
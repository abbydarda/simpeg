<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_m extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }
  
  public function check_login($username, $password)
  {
    $this->db->where('nip', $username);
    $this->db->where('password', $password);
    $response = $this->db->get('user');
    return $response->result_array();
  }
  
  public function get_user()
  {
    $this->db->where('LENGTH(nip)',18);
    $response = $this->db->get('user');
    return $response->result_array();
  }
  
  public function get_user_id($id)
  {
    $response = $this->db->get_where('user', array('id'=>$id));
    return $response->result_array();
  }

}
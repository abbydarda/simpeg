<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_m extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }
  
  public function insert_user($payload)
  {
    $res = $this->db->insert('user',$payload);
    return $this->db->affected_rows();
  }
  
  public function update_user($payload,$id)
  {
    $this->db->where('id', $id);
    $this->db->update('user', $payload);
    return $this->db->affected_rows();
  }
  
  public function delete_user($id)
  {
    $this->db->where('id', $id);
    $this->db->delete('user');
    return $this->db->affected_rows();
  }
  
  public function insert_cuti($payload)
  {
    $res = $this->db->insert('cuti',$payload);
    return $this->db->affected_rows();
  }
  
  public function insert_pengajuan($payload_pengajuan)
  {
    $res = $this->db->insert('pengajuan_cuti',$payload_pengajuan);
    return $this->db->affected_rows();
  }
  
  public function get_data_pengajuan($id = NULL)
  {
    $this->db->select('pengajuan_cuti.*');
    $this->db->select('user.nip');
    $this->db->select('user.nama');
    $this->db->from('pengajuan_cuti');
    $this->db->join('user','user.id = pengajuan_cuti.id_user');
    if ($id != NULL) {
      $this->db->where('id_user', $id);
    }
    $this->db->order_by('tanggal_pengajuan');
    return $this->db->get()->result_array();
  }
  
  public function get_data_pengajuan_id($id)
  {
    $this->db->select('pengajuan_cuti.*');
    $this->db->from('pengajuan_cuti');
    $this->db->where('id', $id);
    $this->db->order_by('tanggal_pengajuan');
    return $this->db->get()->result_array();
  }
  
  public function get_data_pengajuan_by_id($id)
  {
    $this->db->select('user.nama');
    $this->db->select('up.nama as pemimpin');
    $this->db->select('uj.nama as pejabat');
    $this->db->select('user.nip');
    $this->db->select('up.nip as nip_pemimpin');
    $this->db->select('uj.nip as nip_pejabat');
    $this->db->select('user.golongan');
    $this->db->select('user.jabatan_struktural');
    $this->db->select('up.jabatan_struktural as jabatan_pemimpin');
    $this->db->select('uj.jabatan_struktural as jabatan_pejabat');
    $this->db->select('user.tempat_tugas');
    $this->db->select('pengajuan_cuti.*');
    $this->db->select("IF (jenis_cuti = 'tahunan' and id_user = '$id' , count(jenis_cuti), 0) as tahunan");
    $this->db->select("IF (jenis_cuti = 'besar' and id_user = '$id' , count(jenis_cuti), 0) as besar");
    $this->db->select("IF (jenis_cuti = 'sakit' and id_user = '$id' , count(jenis_cuti), 0) as sakit");
    $this->db->select("IF (jenis_cuti = 'bersalin' and id_user = '$id' , count(jenis_cuti), 0) as bersalin");
    $this->db->select("IF (jenis_cuti = 'alasan' and id_user = '$id' , count(jenis_cuti), 0) as alasan");
    $this->db->select("IF (jenis_cuti = 'lain' and id_user = '$id' , count(jenis_cuti), 0) as lain");
    $this->db->from('pengajuan_cuti');
    $this->db->join('user as user','user.id = pengajuan_cuti.id_user','left');
    $this->db->join('user as up','up.id = pengajuan_cuti.id_pemimpin','left');
    $this->db->join('user as uj','uj.id = pengajuan_cuti.id_pejabat','left');
    $this->db->where('pengajuan_cuti.id', $id);
    $this->db->order_by('tanggal_pengajuan');
    return $this->db->get()->result_array();
  }
  
  
  public function update_pengajuan($id,$status)
  {
    $this->db->where('id', $id);
    $this->db->update('pengajuan_cuti', $status);
    return $this->db->affected_rows();
  }
  
  public function get_cuti_user($tahun)
  {
    $this->db->select('user.nip');
    $this->db->select('user.nama');
    $this->db->select('cuti.jumlah_cuti_tahunan');
    $this->db->select('IF(cuti.jumlah_cuti_bersalin is null, 0 , cuti.jumlah_cuti_bersalin) as jumlah_cuti_bersalin');
    $this->db->from('cuti');
    $this->db->join('user', 'user.id = cuti.id_user', 'left');
    $this->db->where('cuti.tahun',$tahun);
    $this->db->where('LENGTH(nip)',18);
    $this->db->order_by('user.nama');
    $response = $this->db->get();
    return $response->result_array();
  }
  
  public function get_sisa_cuti($id)
  {
    $this->db->select('jumlah_cuti_tahunan');
    $this->db->where('id_user',$id);
    return $this->db->get('cuti')->result_array();
  }
  
  public function update_cuti($id_user, $jumlah, $tahun)
  {
    if ($jumlah != 40) {
      $this->db->set('jumlah_cuti_tahunan', "jumlah_cuti_tahunan-$jumlah", FALSE);
    }else {
      $this->db->set('jumlah_cuti_bersalin', "jumlah_cuti_bersalin-$jumlah", FALSE);
    }
    $this->db->where('id_user', $id_user);
    $this->db->where('tahun', $tahun);
    $this->db->update('cuti');
    return $this->db->affected_rows();
  }
  
  public function get_diff_user($tahun)
  {
    return $this->db->query("SELECT id,jk,status_keluarga FROM user where id not in (SELECT id_user from cuti where tahun = '$tahun')")->result_array();
  }
  
  public function get_pemimpin()
  {
    $this->db->select('*');
    $this->db->where('hak_akses',3);
    return $this->db->get('user')->result_array();
  }
  
  public function get_pejabat($id)
  {
    $this->db->select('*');
    $this->db->where('id !=',$id);
    $this->db->where('hak_akses',2);
    return $this->db->get('user')->result_array();
  }

}
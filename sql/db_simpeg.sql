-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 30 Agu 2018 pada 02.23
-- Versi Server: 5.7.23-0ubuntu0.18.04.1
-- PHP Version: 5.6.37-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_simpeg`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `cuti`
--

CREATE TABLE `cuti` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `jumlah_cuti_tahunan` int(11) NOT NULL,
  `jumlah_cuti_bersalin` tinyint(2) DEFAULT NULL,
  `tahun` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `cuti`
--

INSERT INTO `cuti` (`id`, `id_user`, `jumlah_cuti_tahunan`, `jumlah_cuti_bersalin`, `tahun`) VALUES
(1, 1, 12, NULL, '2018'),
(2, 2, 5, NULL, '2018'),
(3, 3, 10, 40, '2018'),
(4, 4, 12, NULL, '2018'),
(5, 5, 12, NULL, '2018'),
(6, 6, 12, NULL, '2018'),
(7, 7, 12, NULL, '2018'),
(8, 8, 12, 40, '2018'),
(9, 9, 12, NULL, '2018'),
(10, 10, 12, NULL, '2018');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengajuan_cuti`
--

CREATE TABLE `pengajuan_cuti` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_pejabat` int(11) NOT NULL,
  `id_pemimpin` int(11) NOT NULL,
  `tanggal_pengajuan` date NOT NULL,
  `tanggal_awal` date NOT NULL,
  `tanggal_akhir` date NOT NULL,
  `jumlah_hari` int(11) NOT NULL,
  `jenis_cuti` varchar(50) NOT NULL,
  `status_pengajuan` tinyint(1) NOT NULL,
  `dinas` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengajuan_cuti`
--

INSERT INTO `pengajuan_cuti` (`id`, `id_user`, `id_pejabat`, `id_pemimpin`, `tanggal_pengajuan`, `tanggal_awal`, `tanggal_akhir`, `jumlah_hari`, `jenis_cuti`, `status_pengajuan`, `dinas`) VALUES
(2, 2, 10, 7, '2018-08-28', '2018-08-29', '2018-08-31', 3, 'tahunan', 1, 1),
(3, 3, 2, 7, '2018-08-30', '2018-08-31', '2018-09-01', 2, 'tahunan', 1, 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nama` varchar(150) NOT NULL,
  `nip` varchar(20) NOT NULL,
  `no_karpeg` varchar(20) DEFAULT NULL,
  `tempat_lahir` varchar(150) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jk` tinyint(1) NOT NULL,
  `status_keluarga` tinyint(1) NOT NULL,
  `agama` tinyint(1) NOT NULL,
  `golongan` varchar(50) DEFAULT NULL,
  `tmt_golongan` date DEFAULT NULL,
  `jabatan_struktural` varchar(100) DEFAULT NULL,
  `jabatan_fungsional` varchar(100) DEFAULT NULL,
  `tmt_jabatan` date DEFAULT NULL,
  `tempat_tugas` varchar(300) DEFAULT NULL,
  `pendidikan_akhir` varchar(100) DEFAULT NULL,
  `jurusan` varchar(100) DEFAULT NULL,
  `lth_penjenjangan` varchar(100) DEFAULT NULL,
  `tgl_capeg` date DEFAULT NULL,
  `mk_kesel_thn` int(11) DEFAULT NULL,
  `mk_kesel_bln` int(11) DEFAULT NULL,
  `mk_golongan_thn` int(11) DEFAULT NULL,
  `mk_golongan_bulan` int(11) DEFAULT NULL,
  `hak_akses` tinyint(1) NOT NULL,
  `email` varchar(300) NOT NULL,
  `foto` text NOT NULL,
  `password` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `nama`, `nip`, `no_karpeg`, `tempat_lahir`, `tanggal_lahir`, `jk`, `status_keluarga`, `agama`, `golongan`, `tmt_golongan`, `jabatan_struktural`, `jabatan_fungsional`, `tmt_jabatan`, `tempat_tugas`, `pendidikan_akhir`, `jurusan`, `lth_penjenjangan`, `tgl_capeg`, `mk_kesel_thn`, `mk_kesel_bln`, `mk_golongan_thn`, `mk_golongan_bulan`, `hak_akses`, `email`, `foto`, `password`) VALUES
(1, 'admin', 'admin', NULL, 'Bandung', '2018-08-01', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'super@user.com', '', 'password'),
(2, 'Hendy Jatnika, IR, MM', '196110021966031010', '-', 'Bandung', '1960-10-02', 1, 2, 1, 'IV/c', '2017-04-01', 'Kepala Dinas', '', '2017-01-02', 'DISTANHOR', 'S2', '', 'SEPAMEN/PIM II', '1986-03-01', 32, 4, 32, 4, 2, 'admin@simpeg.net', 'assets/images/pegawai/2ac7efa697466cf45b15b4282d735605', 'password'),
(3, 'Lina Herlina, SP.,MM.', '197908152010012002', NULL, 'Garut', '1979-08-15', 0, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 'linaherlina@gmail.com', '', 'password'),
(4, 'Dalice Thenu', '196311261992022003', NULL, 'Bandung', '1965-11-15', 0, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 'dalice@gmail.com', '', 'password'),
(5, 'Ibrahim Syaf S.A., S.Sos., M.Si', '196010251986031004', 'E.123219', 'Bandung', '1960-10-02', 1, 2, 1, 'IV/a', '2014-04-01', 'Sekretaris Dinas', '', '2014-01-02', 'Dinas TPH', 'S2', 'ADM. Negara', 'SEPALA/PIM IV', '1986-03-01', 36, 3, 31, 3, 2, 'coba@gmail.com', '', 'password'),
(6, 'Ewon Kodarisman, S.Sos', '196310231986031013', 'D.318343', 'Ciamis', '1970-10-23', 1, 2, 1, 'III/d', '2014-04-01', 'Kasubag Kepegawaian & Umum', '', '2017-07-28', 'Sekretariat Dinas', 'S1', '', '', '1986-03-01', 32, 4, 29, 3, 2, 'coba2@gmail.com', '', 'password'),
(7, 'pemimpin', 'pemimpin', NULL, 'Bandung', '2018-08-01', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 'pemimpin@pemimpin.com', '', 'password'),
(8, 'Wiwi Dewi Setiawati', '196511151993102001', 'C.0727297', 'Bandung', '1965-11-15', 0, 2, 1, 'III/b', '2013-10-01', 'PENGADM.PERJALANAN DINAS', '', '0000-00-00', 'SUB BAG UMUM', 'SLTA', '', '', '1993-10-01', 27, 6, 23, 6, 2, 'wiwi@gmail.com', '', 'password'),
(9, 'Rusmana', '196108161089101002', 'E. 567632', 'Sumedang', '1961-08-16', 1, 2, 1, 'II/a', '2005-10-01', 'PELAKSANA', '', '0000-00-00', 'SUBAG KEPEGAWAIAN & UMUM', 'SD', '', '', '1989-10-01', 36, 9, 29, 9, 2, 'rusmana@gmail.com', '', 'password'),
(10, 'ATEP MUTAQIN', '197407272007011010', 'N.308464', 'GARUT', '1974-07-29', 1, 2, 1, 'II/b', '2018-04-01', 'DOKUMENTASI', '', '0000-00-00', 'SUBAG KEPEGAWAIAN & UMUM', 'SLTA', '', '', '1989-10-01', 36, 9, 16, 3, 2, 'atep@gmail.com', 'assets/images/pegawai/76a2458722440b5510102ae858f0fa0d', 'password');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cuti`
--
ALTER TABLE `cuti`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_user_2` (`id_user`);

--
-- Indexes for table `pengajuan_cuti`
--
ALTER TABLE `pengajuan_cuti`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nip` (`nip`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cuti`
--
ALTER TABLE `cuti`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `pengajuan_cuti`
--
ALTER TABLE `pengajuan_cuti`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `cuti`
--
ALTER TABLE `cuti`
  ADD CONSTRAINT `cuti_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
